# Introduction

<div align="center">

![spaceman-falling-400x301](../Images/spaceman-falling-400x301.gif "I'm coming in fast!")\
</div>

"*What is this game, anyway*?"

It's a natural question, but I'm not sure the answer is mine to give.

I do know that
***Stardrifter*** is *not* a
game that tries to be all things to all people. It is locked to a
specific genre and setting; and though it may be adaptable, it hasn't
been designed with modularity in mind. The basic rule set (the game
*engine*, as it were) is not divorced from the rest of it. If you play 
***Stardrifter***, you're
playing a particular sort of game. This includes its space opera
trappings, but more comprehensively, it covers an approach to
role-playing itself.

I also know this isn't a game for casual players. Certainly, it can be
tailored that way, and one-shot adventures are not only possible, but
very enjoyable. Just as with most games, though, this one only truly
blossoms when the campaign has depth. That means time. That means
players and game masters who put in that time. And in
***Stardrifter***, that
means *character* and *plot* are valued over number-crunching and
dice-rolling. Those things can be fun, and you'll get to do them here,
but they aren't the game.

<div align="center">

![rocketship-at-slight-angle-400x113](../Images/rocketship-at-slight-angle-400x113.gif "Blasting off!")\
</div>

***Stardrifter*** isn't
about complexity for its own sake. Rules that fail to enhance the
experience should be avoided or modified; rules are only tools that
allow the game to be played, and broken tools shouldn't be used. Some of
the rules might not work well for you; some might be a poor fit for your
group's playing style, while others may seem irrelevant, or even
nonsensical. In all circumstances, the rules are less important than the
experience. Your campaign and gaming style should reflect that.

Consequently,
***Stardrifter*** is *not* a
game for people who require a rule for every conceivable circumstance.
No RPG can cover all situations, but this one, especially, assumes that
game masters and players alike are creative, dynamic, interesting
people; folks who delight in taking what an adventure has to offer, and
turning it to their advantage with the whatever is at hand. Skills and
abilities are meant to be interpreted and applied in new, unusual ways.
Situations are intended to be assessed and discussed, and their
solutions negotiated. Players should quiz the NPCs, each other, and the
GM, both in and out of character. They should ask and ask and ask.

So if you pin me down and ask-ask-ask, then I think
***Stardrifter*** is a
dialog. A conversation. It's a place for the dreamer and raconteur alike
to sit and play. It's where adventure, drama, comedy, and tragedy all
live side-by-side. It's horror, ambition, hope.

It's whatever you make of it, so let's get started!
