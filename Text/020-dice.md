# Dice

To create your character in Stardrifter, and play them throughout the
game, you will need a set of **polyhedral dice**; that is, a set of dice
that have **4**, **6**, **8**, **10**, **12**, and **20** sides. These
dice are used to generate random numbers up to the amount of sides each
die possesses. These dice are designated as **1d4**, **1d6**, **1d8**, **1d10**, **1d12**, and
**1d20** respectively. Multiples of each type are (for example) written as 
**2d4**, if you are rolling two four-sided dice; **5d6** if rolling five six-sided 
dice; **3d8** if rolling three eight-sided dice; etc.

Two ten-sided dice rolled together can generate numbers between
**1** and **100**,
making them useful for obtaining random percentages, or other numbers
that might fall into this range. Two ten-sided dice, used in this
manner, are therefore written as **1d%** 
(since they can create random percentile numbers between **1** and **100**), 
and *not* **2d10** (which would, instead, be used to generate 
random numbers between **2** and **20**).

Polyhedral dice may purchased from your local gaming shop, or from many
venders online. Comprehensive usage guides can be found online, by
searching the keywords "*using polyhedral dice*", or something similar.

These dice are quite fun to play with, and very simple to use once you
get the hang of them. There are also game-focused random number apps
available for computers and phones, many of them for free, which can do
the exact same job.

However you go about it, grab yourself polys, and get to rolling!

<div align="center">

![dice-set-400x319](../Images/dice-set-400x319.gif "Polyhedral dice!")

</div>

## Special Rules 

### Rolling Above 100 
Occasionally, in
***Stardrifter***, you may
need to generate random numbers above **100**;
most commonly, for space combat, but possibly other situations, as well.
A few examples of how to do this are as follows:

<div style="font-style: italic">

> **Example**: A charged particle cannon
might do Hit Point damage between **3** and
**300**. This would be designated as
**3d100**. Obtaining this requires that you
roll **1d100** three times, and add up the
results.
</div>

<div style="font-style: italic">

> **Example**: A tactical nuclear missile
might do Hit Point damage between **501**
and **1500**. This would be designated as
**1d1000+500**. You must start by generating
a random number between **1** and
**1000**. This is done by rolling three
ten-sided dice, designating one as the "ones", another as the "tens",
and another as the "hundreds". These three are rolled together, and
read much as you would for percent dice, but with an added number
category. Once the number on the dice is obtained, you then add 500,
to arrive at a final score.
</div>

<div style="font-style: italic">

> **Example**: An in-space collision might do
Hit Point damage between **3** and
**3000**. This would be designated as
**3d1000**. In this case, you would roll
**1d1000** (that is, the same three
ten-sided dice as mentioned in the example above) *three times*, adding each
**1d1000** number together, to obtain a
total. Another way to obtain such a number is to roll
**1d1000**
*once*, then multiply it by
**3**. Ask your GM which method is preferred.
</div>


### Second Chance Rolls
Another rule intrinisic to the game, and used in particular circumstances, is the Second Chance die roll. Certain characters (or, at the discretion of the GM, *any* characters in certain situtations) who fail a **Skill**, **Save**, or **Attribute** check, are allowed, in very specific circumstances, to roll again if they want, in hopes of getting a better number. This is purely optional for them, but if they choose to do it, they *must* accept the result of the second roll, regardless of the number generated, even if it's worse than the first roll.

Second Chance rolls are not adaptable from one statistic to another; if a character is allowed Second Chance rolls for their **Skills**, for instance, they do not get Second Chances on their *Attributes** and **Saves** as well.

Additinally, ***Second Chance*** rolls cannot be stacked; that is, if a character is allowed more than one ***Second Chance*** rolls in a situation. ***Second Chance*** does not mean Third or Fourth Chance.

<div style="font-style: italic">

> **Example**: Gia has the **Groundpounder** Background, and is, therefore, allowed **Second Chance** rolls on all her Skills whenever she's on a planet. During the adventure, she lands on a terraformed
world, and finds out she needs **Engineering** to stop a dam from bursting. If she has Engineering as a Skill, she can get a **Second Chance** on her roll for it. If she doesn't have Engineering, she can make a substitute Attribute roll, either INT or WIS, with modifiers, but she does not get a **Second Chance** roll, because she is not using a Skill. If she has Engineering as a **Prime Skill**, she may make only one **Second Chance** roll in the attempt to fix the dam, not two, even though she has a 
**Second Chance** for being on a planet, and another for Engineering.
</div>
