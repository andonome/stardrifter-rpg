# Rolling Up A Character

## Quick Overview
Some of what follows in this section might seem abstract compared to
other RPGs, but a basic philosophy of ***Stardrifter*** is that a
character's statistics are just numbers, and numbers are *not* your
character. Numbers only represent what they *might-maybe-possibly-who
knows?* be able to do in certain situations. **This cannot be stressed highly
enough.** Don't look to the rules for permission to try something. Don't look to
your Character Sheet for the right combination of Skills, Attributes, or
bonus numbers. The right way to get things done is almost always in your
imagination.

<br />

<div align="center" style="font-size: 20px">

***Numbers aren't your character. How you play is your character.***

</div>

<br />

There is a blank Character Sheet at the end of this book which you can
copy out and use. It will help to have this in front of you as you go
along, as well as a piece of scrap paper.


<br />
<div align="center">

![man-shooting-raygun-500x206](../Images/man-shooting-raygun-500x206.gif "Have a taste of my atomic ray gun!")\

</div>
<br />

## First Off, Don't Worry
***Stardrifter*** isn't a "crunchy" game, in the sense of the amount of steps or variables in the
character creation process or regular gameplay. There are only a few elements, here and there, that deliberately add complexity, and these are entirely optional. If the rules seem difficult to follow, then they must be poorly written, because you are *more* than capable of doing this!

## Who Are You?
Pick a name, age, and gender (if applicable) for your character. Imagine
what they look like. Are they tall, short, thin, full-figured, or simply
average? Are they dark-skinned? Light-skinned? Have they had their hair
or skin dyed to an exotic color? Understanding that physical beauty is
highly dependent upon cultural norms, are they objectively good-looking?
Do they have scars, piercings, tattoos or *glowtoos* (self-illuminating
skin art)?

Within reason, your character can have any appearance you want. They can
have any religious or ethnic background. This is a future where anything
goes, and you are not bound by 21st Century limitations, values, or
inhibitions. All the important numeric aspects of a character are on one
side of the Character Sheet. This leaves the other side blank, and ready
for your character's story. Write down the details of their appearance,
and, as you go along in the creation process to determine their other
characteristics, you'll have an even larger tapestry to work from. Don't
finalize their backstory just yet, since certain aspects of the creation
process to come may give you some good ideas!

<br />

## Overview

What follows is a list of the steps needed to create your
***Stardrifter*** character.
It describes what the next few chapters cover, and in what order to
proceed. One good way to go through the list is to read a step, then
turn to the chapter in the book that it refers to, follow the
directions, and then return here. You'll find yourself working through
the list quickly, and when you are done with it, your character is ready
to play.

### Step 01: Attributes
Decide which method you want to use to generate your character's
**Attributes**, and the optional *Initiative Differential* (**ID**)
for each. There are several methods described for obtaining these
numbers.

Do you have a vision of your character that you want to build toward?
Perhaps, you would prefer to let chance dictate the character's
Attributes, and then challenge yourself to assemble a cool person out of
them? Or maybe the Attributes don't really matter that much to you. If
so, there's an option to roll *one* die for some perfectly serviceable
pre-rolled statistics. All of these methods will generate viable
Attributes for your character.

### Step 02: Saves
From the Attributes, generate your **Saves**, and (if you want) their
optional IDs. Saves are last-ditch numbers you'll roll against in
certain situations, where success or failure truly matter. This includes
life and death stuff, but also, more mundane or unexpected things. Other
choices you make as you create your character, and definitely, as you
play them, which can add to or subtract from your Saves. High or low
numbers here may make a difference sometimes, but they don't make or
break a character. Again, how you play matters more than what you roll.

### Step 03: Background
Choose a **Background** for your character. Backgrounds have a couple of
purposes in the game. Right off the bat, they offer some free Skills
(which we'll cover in Step 05, so don't sweat it just yet). These
freebies are of a limited selection, for those with a similar starting
point in life. Backgrounds influence your character's base HP and
Stamina (described in **Step 04**). They can offer up particular advantages,
as a reflection of one's upbringing or personal history. Finally,
Backgrounds point the way toward other Skills that you may wish to
choose (or not), when the time comes.

### Step 04: Hit Points and Stamina
Determine your character's **Hit Points** and **Stamina**. Collectively,
these scores are a stylized reflection of how much physical damage your
character can take before dying. They can be increased (or decreased)
over time, depending upon the choices you make as you progress in the
game. Every character starts with **8 Hit Points (HP)** and **8 Stamina
(STAM)**, but your Attributes can, and your Background definitely
*will*, modify those numbers. Once again, don't get too hung up over
high numbers here. Having lots of HP and STAM are advantages, but they
simply can't replace smart in-game choices. A character caught in a
firefight needs all the HP they can get; one who *avoids* the fight
might not need so much.

### Step 05: Skills
Choose your **Skills**, and their optional IDs. In addition to a number
of free Skills, based upon your character's Background, each new
character gets **4** Character Points (**CP**) at rolling-up time, with which to
obtain additional new Skills. You can use these to help realize your
vision for this character. What do you want this person to be like?
Based upon the history you came up with, what are their goals? Do you
know if there is there a Skill deficiency among the player characters as
a whole? Keeping all these things in mind, now is the time to choose.
Depending upon your Background, you may also be able to choose a Prime
Skill, which offers certain advantages. This game is largely
Skill-based, so giving this step some time and attention is vital.

### Step 06: Money
Your starting **Money** is a type of future currency called the
***Q***. Player characters start the game with 2500***Q***, with which they
can purchse Weapons, Armor, and other stuff. It's important to
understand how much money your character has available, especially in
advance of **Step 07**.

### Step 07: Equipment
Now that you have some money in your pocket, consult the list of your
chosen Skills, and purchase any **Equipment** that will help you perform
them (assuming you can afford it just yet). In Stardrifter, Equipment
includes gadgets and gizmos, but also Weapons and Armor. Alternatively,
you can wait and save your ***Q*** until you know what sort of equipment
you'll need for a particular adventure, and buy it then. Travel expenses
are a consideration; tickets from one system to another can sometimes be
expensive. There are basic living costs associated with this future
time, nwhich is a number that varies from location to location; a
run-down station, in an out-of-the-way star system, might have a low
living cost, but it will likely be harder to find work, or to even buy
the Equipment you need.

<br />
<div align="center">
![planet-in-shadow-300x300](../Images/planet-in-shadow-300x300.gif "An unknown world, full of wonder and danger!")\
</div>
<br />

And that's it. Not too bad, right? Your character is now ready to move
across the stars, discovering secrets, opposing enemies, and (possibly)
surviving to tell the tale!
