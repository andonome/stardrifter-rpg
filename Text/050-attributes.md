# Attributes

Attributes are an abstract representation of both the physical and
mental aspects of a character. You will find them listed on the bottom
right-hand side of your Character Sheet. ***Stardrifter*** uses the
classic six statistics, as follows:

> **Intelligence** (INT): The basic ability to understand things, and
> figure out solutions to problems, especially under pressure.
>
> **Wisdom** (WIS): General insight into situations and human behavior.
>
> **Charisma** (CHA): Personal presence, and an ability to impress
> people.
>
> **Strength** (STR): Physical power. Each point of STR allows a
> character to lift up to 10 kilograms from floor to chest height, under
> 1 Terran gee of gravity.
>
> **Constitution** (CON): Physical fortitude.
>
> **Dexterity** (DEX): Overall nimbleness and coordination.

<br />
<div align="center">

![kid-floating-in-space-400x352](../Images/kid-floating-in-space-400x352.gif "Golly-gee! It's a long way down!")\

</div>
<br />

## Obtaining Attributes

There are three general directions to go in with this. Any of these
methods will result in viable Attributes.

1.  Use a pool of Character Points (**CP**s), from which you allocate some to each Attribute.
2.  Roll dice, and assign the numbers where you like (and there are several different methods you can try).
3.  Roll one die to obtain a set of pregenerated Attributes from a list.

However you go about it, once you have the these numbers, place them on
your Character Sheet under **Attributes**, in the bottom right corner.

<br />

**1.) Pool of Character Points**

You start with a pool of 75 CPs and allocate them as you wish among your
Attributes. All six Attributes must get some points, with a minimum of
3, and a maximum of 18. This will allow for better-than-average
Attribute scores all around, or for a couple of truly great numbers
along with a few that are less impressive.

One advantage of this method is that a character may be more-or-less
tailored to your vision. If you want a pilot, putting extra points into
DEX is probably a good idea. If you want to create a fighting type,
putting more points into STR or CON could be useful. Granted, you might
have to short-change other Attribute scores to get those high numbers,
but, hey -- everything has a cost.

<br />

**2.) Roll Dice** (Use one of these dice-rolling methods, or another of
your GM's preference.)

-   ***Method 01***: Roll **3d6** and add them together. Roll them a total of
    six times, placing each number under any Attribute on your Character
    Sheet that you want. At that point, you may (but do not have to)
    bring the lowest number under up to 10.
-   ***Method 02***: Roll **4d6**, dropping the lowest die. Add the remaining
    dice together. Do this a total of six times, and place the final
    numbers under any Attribute on your Character Sheet that you want.
-   ***Method 03***: Roll **3d6** and add them together. Roll them a total of
    six times, placing each number under any Attribute on your Character
    Sheet that you want. You may then (and *only* then) reroll any or all of these numbers, but you *must* accept the second roll, whatever it is, and you cannot swap the numbers around. Whatever you roll at this point, for whichever Attribute, is what you keep.

**3.) Roll One Die**

The following is a table full of random Attribute numbers. Roll
**1d20**, and consult the following list for your character's
Attributes, and Saves. Place these numbers on your Character sheet under
**Attributes** and **Saves**, in the lower right corner. These scores
have been obtained using the above methods. Additionally, each number
has its Initiative Differential (**ID**) included, which is used with
the ***Advanced Initiative*** rules. If your GM is not using these
rules, the ID numbers may be ignored. (See below.)

<div style="page-break-after: always;"></div>
| ROLL 1d20 | **INT**/ID |  WIS/ID | CHA/ID | STR/ID | CON/ID | DEX/ID  | SAVE: Mental | SAVE: Physical |
| :---:     | :---:  | :---:   | :---:  | :---:  | :---:  | :---:   | :---:        | :---:          |
| 01        | 13/07  | 12/08   | 10/10  | 15/05  | 12/08  | 13/07   | 12           |  13            |
| 02        | 15/05  | 12/08   | 13/07  | 10/10  | 14/06  | 11/09   | 13           |  12            |
| 03        | 08/12  | 10/10   | 11/09  | 16/04  | 15/05  | 15/05   | 10           |  15            |
| 04        | 13/07  | 09/11   | 11/09  | 13/07  | 13/07  | 16/04   | 11           |  14            |
| 05        | 12/08  | 11/09   | 17/03  | 10/10  | 13/07  | 12/08   | 13           |  12            |
| 06        | 10/10  | 06/14   | 11/09  | 10/10  | 11/09  | 10/10   | 09           |  10            |
| 07        | 12/08  | 08/12   | 11/09  | 07/13  | 07/13  | 10/10   | 10           |  08            |
| 08        | 09/11  | 07/13   | 11/09  | 10/10  | 12/08  | 11/09   | 09           |  11            |
| 09        | 13/07  | 14/06   | 10/10  | 06/14  | 14/06  | 13/07   | 12           |  13            |
| 10        | 13/07  | 14/06   | 10/10  | 09/11  | 12/08  | 12/08   | 12           |  11            |
| 11        | 11/09  | 14/06   | 13/07  | 10/10  | 16/04  | 13/07   | 13           |  13            |
| 12        | 06/14  | 10/10   | 18/02  | 17/03  | 13/07  | 08/12   | 11           |  13            |
| 13        | 11/09  | 08/12   | 09/11  | 13/07  | 16/04  | 11/09   | 09           |  13            |
| 14        | 11/09  | 09/11   | 13/07  | 15/05  | 16/04  | 11/09   | 11           |  14            |
| 15        | 17/03  | 11/09   | 11/09  | 10/10  | 13/07  | 11/09   | 13           |  11            |
| 16        | 11/09  | 14/06   | 13/07  | 10/10  | 16/04  | 13/07   | 13           |  13            |
| 17        | 09/11  | 15/05   | 12/08  | 13/07  | 14/06  | 11/09   | 12           |  13            |
| 18        | 10/10  | 17/03   | 13/07  | 11/09  | 15/05  | 14/07   | 13           |  13            |
| 19        | 13/07  | 11/09   | 08/12  | 10/10  | 09/11  | 11/09   | 11           |  10            |
| 20        | 12/08  | 14/06   | 15/05  | 14/06  | 16/04  | 16/04   | 14           |  15            |

<br />
<div align="center">

![astronaut-retro-250x221](../Images/astronaut-retro-250x221.gif "I'm being pulled into a space vortex!")\

</div>
<br />

### Initiative Differential

The **InitDiff**, or **ID**, is a number based upon an Attribute or
Skill score. You'll find a space for them next to each of these scores
on the Character Sheet.

ID is used for the optional ***Advanced Initiative*** game mechanic that
allows for finely-tuned combat sequences. ID acts as a modifier upon the
standard Initiative roll, reflecting how good or bad a particular score
is. If *Advanced Initiative* is used, and the GM calls for an ID for a
particular Attribute, Save or Skill for determining the order of
initiative, the player rolls **1d20**, and adds that ID number to the
die roll. Lowest number goes first, then the next lowest, and so on.

To obtain IDs, take the Attribute or Skill score, and subtract it from
**20**.

> **Example**: *Sopel, a new adventurer, has the following Attribute
> scores, and the derived IDs for each:*
>
>-   *INT 12 **ID 8** (20-12=8)*
>-   *WIS 10 **ID 10** (20-10=10)*
>-   *CHA 14 **ID 6** (20-14=6)*
>-   *STR 11 **ID 9** (20-11=9)*
>-   *CON 16 **ID 4** (20-16=4)*
>-   *DEX 12 **ID 8** (20-12=8)*

Very high Attributes, obtained over time, may exceed **20**, but they
are still calculated and used in the same way. Since lower is better for
Initiative, very low IDs are also better.

> **Example**: *After a lot of adventuring, Sopel now has the following
> Attributes and IDs:*
>
>-   *INT 13, ID 7 (20-13=7)*
>-   *WIS 12, ID 8 (20-12=8)*
>-   *CHA 18, ID 2 (20-18=2)*
>-   *STR 15, ID 5 (20-15=5)*
>-   *CON 22, **ID -2** (20-22=-2)*
>-   *DEX 20, **ID 0** (20-20=0)*

Again, the ID game mechanic is for use with an *optional* rule, which
means it is also optional. Check with your GM to see if they will be
using ID in their campaign; if not, you may safely ignore it. (See the
chapter on **Combat** to see examples of how ID is utilized in the
game.)

<br />
<div align="center">

![classic-rocket-flying-250x149](../Images/classic-rocket-flying-250x149.gif "The new rocketship is handling beautifully!")\

</div>
