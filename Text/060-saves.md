# Save Checks

**Save Checks** (or just Saves) are last ditch
numbers to roll against in order to reduce or avoid the effects of bad
and otherwise unavoidable situations. This can refer to almost anything,
ranging from explosions, to surviving a spell of vacuum exposure without
a suit; from being conned by a grifter, to resisting the effects of
brainwashing; from failing a written exam, to ending a blind-date on a
sour note.

Like other types of Checks (Attribute and Skill), Saves are rolled with
**1d20**. Equal to or less-than means success; greater-than means
failure; and modifiers may well apply.

Save Checks come in two varieties, 
**SAVE: Mental** and 
**SAVE: Physical**. Their scores are obtained as
follows:

-   **SAVE: Mental**: Add up your character's INT, WIS, CHA, and divide
    by 3 (rounding up). Put this number under SAVE: Mental, in the lower
    right corner of your Character Sheet.
-   **SAVE: Physical**: Add up your character's STR, CON, DEX, and
    divide by 3 (rounding up). Put this number under SAVE: Physical, in
    the lower right corner of your Character Sheet.

Save Check scores can be improved over time by increasing a character's
Attributes via CPs, which are obtained through adventuring. When an
Attribute is permanently increased (or *decreased;* it's a dangerous
galaxy), Save Check scores must be re-calculated using the above
methods.

> **Example**: *An enby character named Bea tries climbing the perimeter
fence of an enemy compound. It's in a remote spot on a mountaintop.
Unfortunately, it is raining this day, and there are also high winds.
The GM imposes a total penalty of -4 (that's -2, each, for the rain
and the wind). Bea rolls a 12 on their Skill Check, against a
**Athleticism 01** score of 14, which would normally be a fine
success. Because of that -4 penalty, though, Bea's modified Skill
score is actually 10. The attempt is a failure. What's worse, the GM
determines that, as they're falling, the high winds blow them right
over a nearby precipice! This would normally be the end, but the GM
decides that, since it's essentially an unavoidable doom, Bea gets a
**SAVE: Physical** Check to see if they can
grab hold of some roots growing right near the cliff. The number under
**SAVE: Physical** on Bea's Character Sheet is 14. The GM decides to
be more lenient than I would probably be under the circumstances, and
does not impose any negative modifiers to this Save. Bea's player
rolls **1d20** and gets a 9, which is lower than 14, meaning this is a
success. The GM does hand out falling damage to Bea (when they slam
into the cliff side while holding on to the roots), but decides they
have prevented themself from falling off the mountain. Bea can now
crawl to safety.*

> **Example**: *It turns out the GM is* not *nicer than I am, after all,
because Bea's brush with death has left some trauma. They've begun
having nightmares about falling, growing increasingly acrophobic. Time
goes by, and dire circumstances within the game require Bea to once
again scale that same fence, this time to save a friend. It is raining
and windy this day, just like last time: what a miserable place! As
before, a successful **Skill Check**, with appropriate modifiers is
needed to actually climb the compound's fence. This time, though,
Bea's fear of heights, coupled with the weather, is a major issue; the
GM determines that Bea must first make a [Save:
Mental]{style="font-weight: bold;"} Check, to push past this fear. The
player rolls a 10. This is more than the **SAVE: Mental** score on
Bea's Character Sheet, which is a 9. That's a fail. Bea is too
overcome with anxiety to even make the attempt, despite how desperate
the situation is.*

> **Example**: *Later on, the weather abates; the rain finally stops,
and the wind dies down. Circumstances with this fence have changed, in
other words, so the GM rules that Bea may make another attempt to
climb it. Their fear of heights is still very much present, so another
**Save: Mental** is required. This time, Bea rolls **1d20**, and gets
a 7. That's under their **SAVE: Mental** score, making this a success.
Bea overcomes their fear, and begins climbing. Now they must make a
Skill Check versus their **Athleticism 01** score of 14. Bea rolls a 9
on **1d20**, and very handily climbs the wall once more. Now...to save
their friend!*
