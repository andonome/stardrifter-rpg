# Background

Backgrounds have a couple of purposes in the game. They offer some free
**Skills** right off the bat, based on common
experiences that anyone with a similar starting point in life might
have. They help determine your character's **Hit Points** and
**Stamina** will be. Finally, they can point the way toward *other*
Skills you may wish to choose, since characters with particular
histories may be prone to particular professions.

Where did your character come from? What sort of path did their early
days put them on? What effect did it produce, mentally and physically?
And what have they learned along the way? Backgrounds are tools used to
determine some of these things, which in turn, can affect others.

Most Backgrounds allow the character to choose a **Prime Skill**, either
from the free ones they get, or from anything else on the Skill list for
which they are eligible. <ins>Once chosen, Prime Skills cannot be changed</ins>*.* 
They do, however, offer certain advantages, not the least of which is them starting
the game at a higher Skill level than is otherwise possible.

<div align="center">

![scifi-city-350x336](../Images/scifi-city-350x336.gif "A steaming metropolis!")\

</div>

Free Skills do not cost anything; the character has them as a matter of
course when they are rolled up. Later on, after adventuring and gaining
more CPs, players may use them to increase these Skills, just like any
others. They can also learn new Skills if they want. And again, a free
Skill may be set as a Prime Skill, if desired.

Backgrounds have an influence on characters at the beginning of the
game, but less so as the things go along. The more a character
adventures through the galaxy, the more their choices shape them into
different people, possibly people who are very far removed from their
beginnings. Backgrounds aren't *Professions* or *Classes*; they don't
dictate how a character will do things, they are merely jumping-off
points.

A Background can be an easy handle in the beginning. Saying to everyone
during the first game, "*Hi, I'm Kinn, a Stationer*," 
might give them a sense of where you're coming from. After a few games, though, 
and a few Skill choices, it might be more appropriate to say, 
"*Hi, I'm Kinn, a pilot. I grew up on a station, but I haven't been back there in a while*."

In Stardrifter, Backgrounds are *only* that. They help determine who you
were, not who you are, and definitely not who you will be.

<div align="center">

![space-scientists-150x170](../Images/space-scientists-150x170.gif "I wonder what will hapen if I press THIS button?")\

</div>

------------------------------------------------------------------------

## Background Types

### Spacer

Spacers, as the name implies, travel through the vacuum of outer space
for their living. Specifically, they are people with civilian
backgrounds who have worked on commercial vessels before. These might
include cargo haulers, cruise liners, communications starships, repair
boats, private yachts, personal security vessels, emergency response
vehicles, and more.

Both starships and spaceboats are the province of Spacers. They know how
to use vacuum suits, and can usually move around and work in zero
gravity with confidence. They can often maintain, repair, or even
improvise technical solutions for damaged space vessels. They can also
apply those Skills elsewhere. Spacers can commonly handle freight safely
and securely, using loadbots, pallet jacks, and lifters. And they
typically deal with a dizzying number of regulations and civil servants
found in ports-of-call throughout the stars.

Spacers get the following free Skills, at Level 01 in each (write them
down on your character sheet, under Skills):

-   **Exosuit**
-   *Either* **Engineering** *or* **Cargo Handling** *or* **Stewarding** (*pick one*)
-   *Either* **Bureaucracy** *or* **Social Engineering** (pick one)


If the player chooses one of these as their character's Prime Skill,
they start the game at Level 02 in that Skill, for free.

As listed later in the section on **Hit Points and Stamina**, Spacers
get **+2 HP** at rolling-up time.

#### Special Abilities

<div style="font-style: italic">

> **1.)** Working in space can be dangerous.
It's said by some that an old Spacer is a lucky Spacer. To simulate
this, Spacers get a **Second Chance** on any **SAVE: Mental** or **SAVE: Physical** 
rolls they need to make. This means, whenever a Save check of some kind is called for, 
the Spacer's player may, at their option, choose to ignore the result, and
immediately re-roll the **1d20**. They must, however, accept the second result, whatever it is.
</div>

<div style="font-style: italic">

> **2.)** Spacers have extensive training and
experience with survival in space. As a result, characters with this
Background who are directly exposed to hard vacuum, or vacuum-like
conditions while not wearing some sort of exosuit, only take
**1/2** STAM damage (*not* HP)  Additionally, Spacers exposed
to vacuum make their **SAVE: Physical**
rolls versus **Stun** at only a
**-2** modifier, instead the usual **-5**.
</div>


------------------------------------------------------------------------

### Stationer 

Stationers hail from one of the many artificial communities in space,
likely a large colony structure of some kind. Most of these places have
an urban quality to them, with large populations packed into small
areas. As such, Stationers tend to be savvy, understanding people and
their processes. They come from communities where knowing who to go to
when you need something can mean the difference between success or
failure.

Though all stations are different, they do still have certain elements
in common, and Stationers can often get along as easily on one as as
they can another. Knowing certain technical Skills allows a Stationer to
obtain employment, and to get things done. They might work as civil
servants, computer programmers, health and medical technicians. They
might be real estate brokers, renting apartments; a nurse, dispensing
medication; plumbers, fixing water heaters and sinks; or con artists,
bilking tourists out of their ***Q***

Stationers get the following free Skills, at Level 01 in each (write
them down on your character sheet, under Skills):

-   **Computers**
-   *Either* **Bureaucracy** *or* **Social Engineering** *or* **Lying** (*pick one*)
-   *Either* **Engineering** *or* **Medico** (*pick one*)

If the player chooses one of these as their character's Prime Skill,
they start the game at **Level 02** in that Skill, for free.

As listed later in the section on **Hit Points and Stamina**, Stationers
get **+1 HP** and **+1 STAM** at rolling-up time.

#### Special Abilities 

<div style="font-style: italic">

> **1.)** As the name implies, Stationers are
most comfortable working upon space settlements. To simulate this,
Stationers get ***Second Chance** rolls on any
of their **Skill** checks, should they want
them, while in or on a space station of some sort. This means,
whenever a Skill check of some kind is called for while the character
is on a station (GM's determination as to what, exactly constitutes a
*station*, as opposed to some other sort
of settlement), the Stationer's player may, at their option, choose to
ignore the result, and immediately re-roll the
**1d20**. They must, however, accept the
second result, whatever it is, even if it's worse than the first.
</div>


<div style="font-style: italic">

> **2.)** Stationers are familiar with the
general structure of space settlements, both from a practical and
social perspective. As a result, a Stationer may make an INT
**Attribute** check to see if they have some
degree of general knowledge, right off the top of their head, about
pretty-much any aspect of space stations or the people living in them.
If successful, this can, at the GM's discretion, garner some small
piece of relevant information or minor trivia apropos to the
situation. Such info might range anywhere from the probable location
of civic buildings, based on common layouts for stations, to where
service tunnels tend to have access points; from where the ritzy or
shady parts of town are probaby located, to whether or not a clerk
might be amenable to bribery, based on how well or poorly paid people
with their position on other stations tend to be. The GM will
determine what can or can't be learned on a case-by-case basis, but a
little imagination can go a long way here.
</div>


------------------------------------------------------------------------

### Xmil

Xmil (pronounced *ex-mill*) characters were once part of a standing
military or paramilitary body of some sort. Exactly how and when this
came about is up to the player, but at one time, this character was
trained to help protect one of the nations of space, a corporate entity,
a religious order, or some other organization. Working long hours,
performing difficult labor under bad conditions, communal living, and
the ever-present threat of combat have all trained this person for
survival while working as part of a team, and surmounting hardships in
many different sorts of environments. Xmil characters were taught how to
obey orders, to respect the chain of command, and to achieve their
goals, despite outside circumstances.

The military has taught this character several basic, but very practical
Skills; as such, Xmils are often sought after for the ranks of mercenary
bodies, police forces, and private security firms. While some go in for
these sorts of jobs after they muster out (or desert), the majority
actually enter different lines of work, unrelated to their military
careers. Those old Skills remain, though, and can be called upon
whenever needed.

Xmils get the following free Skills, at Level 01 in each (write them
down on your character sheet, under Skills):

-   **Exosuit**
-   **Combat** (*choose which*)
-   **Survival** (*choose which*)

If the character takes one of these free Skills as their Prime Skill,
they start the game at **Level 02** in it.

As listed later in the section on Hit Points and Stamina, Xmils get **+1
HP** and **+1 Stamina** right off the bat.

#### Special Abilities 

<div style="font-style: italic">

> **1.)** Xmils have been trained to be aware
of dangerous or significant precarious situations in their
environment. They often scan for exits and defensible positions
whenever they walk into a room; they are able to recognize
particularly good places for ambushes, or retreats; and they can often
notice hidden threats before others do. To simulate this, Xmil
characters may roll for ***Tactical Insight***, by making a
successful **SAVE: Mental** in order to
recognize a situation for what it is, from a military standpoint. An
Xmil character may use Tactical Insight whenever they want, however,
no failed rolls may be retried until something significantly changes
about the situation in question (GM's determination). The type, and
amount. of information garnered through Tactical Insight is entirely
up to the GM, as are any situational modifiers to the 
**SAVE: Mental** score being rolled against.
</div>


<div style="font-style: italic">

> **2.)** Xmil characters are especially adept
at fighting; they've been trained under live-fire conditions, and have
learned how to endure pain. All other character types, when they take
**HP** damage (*not* Stamina), roll a **SAVE: Physical** in order to
take 1/2 damage (see the chapter on **Damage and Healing**). In this same situation,
however, Xmil characters roll a **SAVE: Physical**, and if successful,
only take **1/4 damage** off their HP (rounded up, minimum of 1 point).
</div>

------------------------------------------------------------------------

### Groundpounder 

Groundpounders are from a planet, either Terra itself, or one of the
other settled worlds in the galaxy. Only some of these have yet been
terraformed, meaning that most have harsh environments. Groundpounders
have been raised under Earth-like gravity, and have had exposure to
varying degrees of solar radiation on a regular basis. Groundpounders
are generally more in tune with environmental situations and
requirements than are others (*e.g.*, checking the weather report before
venturing out; understanding what to do in quakes or storms; or even
just knowing how to swim).

Groundpounders can hail from cities on a planet's surface, but they are
also familiar with the open country and wilderness to a degree. They
know about the dictates of a rising and setting sun, and oftentimes have
a grasp on scientific principles and methodology from an entirely
practical standpoint. Much of the population upon terraformed worlds
tend to be homesteaders, people capable of survival under harsh
conditions. Groundpounders bring this hardiness to their adventuring
lives.

Groundpounders get the following free Skills, at Level 01 in each (write
them down on your character sheet, under Skills):

-   **Survival** (choose which)
-   **Science** *or* **Engineering** (pick one)
-   **Exosuit** *or* **Athleticism** (pick one)

If the player chooses one of these as their character's Prime Skill,
they start the game at **Level 02** in it.\

As listed later in the section on **Hit Points and Stamina**,
Groundpounders get **+2 Stamina** right off the bat.

#### Special Abilities 

> **1.)** Groundpounders feel centered with
their feet on solid earth. To simulate this, while on planetary
bodies, moons, or asteroids of significant size (GM's determination),
they get ***Second Chance*** rolls on any
of their **Skill** checks, should they want
them. This means, whenever a Skill check of some kind is called for
while the character is on one of these bodies, the Groundpounder's
player may, at their option, choose to ignore the result, and
immediately re-roll the **1d20**. They must,
however, accept the second result, whatever it is, even if it's worse
than the first.

> **2.)** Famously hardy, Groundpounders can
take a few more hits than most folks before really feeling the pain.
They can shake their heads and just keep going, when others might be
down for the count. To simulate this, Groundpounders get a
**+2** modifier on any and all **SAVE: Physical** rolls versus
**Stun**.

------------------------------------------------------------------------

### Drifter

Drifters are people who come from everywhere and nowhere. They travel
the galaxy, perhaps from job to job, or by flying first class on
passenger liners, or even by stowing away on freighter ships, little
more than hobos. They can be found on ships, boats, stations, and
planets, but rarely in any one place for very long. They have friends
(and enemies) everywhere, but few obligations to hold them down. They
know how to deal with authority, how to get jobs and information, and
how to survive in a galaxy indifferent to human triumph and failure.

Drifters often learn a little about a lot of different things. They can
become experts in a subject, just like anyone else, but a wandering
lifestyle instills a wide breadth of basic knowledge, often emphasizing
adaptability and readiness over specialization. Drifters can be tramps
and look the part; but they can also be traveling sales reps in nice
suits; hired muscle; landless Noblefolk; or seasonal migrant workers on
a series of agricultural worlds. They can be *all* these things, in
fact, and much more, doing whatever it takes to get by, wherever the
winds of fate might bring them.


Drifters get the following free Skills, at Level 01 for each (write them
down on your character sheet, under Skills):

-   **Bureaucracy**
-   **Exosuit**
-   **Survival** (choose which)
-   *Either* **Cargo Handling** *or* **Engineering** *or* **Science** *or* **Stewarding** (pick one)
-   *Either* **Social Engineering** *or* **Lying** (pick one)

Drifters get more free Skills than other characters, but they do *not*
get a Prime Skill.

As listed later in the section on **Hit Points and Stamina**, Drifters
get **+1 HP** and **+1 Stamina** right off the bat.

#### Special Abilities

<div style="font-style: italic">

> **1.)** Drifters learn quickly, but not
always to great depth. If a Drifter observes someone perform a
**Skill** for at least one round which they,
themself, do not have, they can attempt to replicate what they've just
seen by performing an **Attribute** Check *without the usual negative
modifiers*. They must attempt to
copy what they've seen immediately, within one minute's time, in order
to gain this advantage, otherwise they have all the same negative
modifiers as anyone else who might try this. If the Drifter stops
performing the Skill, even for one round, or is asked to apply the
Skill in a different way than what they saw performed, they also lose
this advantage, since they've only been aping someone else.\
</div>

<div style="font-style: italic">

> **Example**:
**[Huppert** is a Drifter, who sees
himself as an itinerant artist, traveling the galaxy, painting and
drawing. He's a free soul, in other words, generally avoiding
trouble. One day, trouble finds *him*: he and his close friend **Mary-Ellen**,
who is a Fleet veteran (**Xmil**), take a
wrong turn, and just happen to wander into a mob war shootout! They
can't get away, and are being actively targeted by gangsters, who
are shooting at everyone they see. the two travelers pick up rifles
from a couple of fallen criminals, but, while Mary-Ellen has her old
military training to fall back upon, Huppert doesn't. He has no
**Combat: Rifle** Skill to use (or any
other **Combat** Skills, for that matter;
he's a lover, not a fighter). In order to shoot the rifle, he would
normally have to roll an **Attribute Check** 
versus his **DEX**, with a wicked GM-assigned
**-5 modifier**, due to general ignorance of
the weapon, and his natural disposition. Instead, Huppert's player
declares that he watches Mary-Ellen fire her rifle for one round of
the combat. This allows him to copy what she does, and attack
without the **-5** modifier. Instead, all
his attacks for this fight are now just straight-up 
**Attribute Checks** for his
**DEX**, with no penalty for ignorance. If
he holds on to the rifle, and uses it again later in another fight,
he will do so with the **-5** penalty
unless Mary-Ellen (or someone else) is right there for him to copy
again.
</div>


<div style="font-style: italic">

>**2.)** Because their interests tend to
shift as often as their locations, Drifters may choose
*one* of their known **Skills** to focus upon
per day. This allows them to get ***Second Chance*** rolls for a
**24 hour** period with that Skill. At the
end of that time, the Drifter's attention inevitably turns to
something else, and they can either choose another Skill to focus on,
or no Skill at all. They cannot choose the same Skill two days in a
row. The GM may require the player to announce the Skill chosen at the
start of the day, or may simply allow them to choose it on-the-fly.
</div>


<div align="center">

![rocket-on-planet-250x123](../Images/rocket-on-planet-250x123.gif "Commencing planetary exploration!")\

</div>
