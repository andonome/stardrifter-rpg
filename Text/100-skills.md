# Skills

By now, you may have an idea about what sort of character you'd like to
play. Perhaps you'll even know what kinds of characters the other
players intend to play. If at all possible, choose those Skills that
will lend shape to the gaming group. Depending upon your Background, you
may also be able to choose a Prime Skill, which offers certain
advantages. As this game is largely Skill-based, giving this step some
time and attention is vital.

An important concept in Stardrifter is that each Skill possesses a
**Skill level**, and a **Skill score** is a reflection of the training and experience a character has in a Skill; *score* is a reflection of the
character's actual competance. Two people may easily undergo the exact
same training (Skill level), yet not end up with the same mastery (Skill
score). A character starts any new Skill at **level** 1, but has a
**score** based upon that Skill's associated Attribute *plus* their current Skill level.

> **Example**: Two friends, **Ghoman** and **Terri**, take a computing class together,
> looking to get ***Computers*** as a new Skill. Computers has an associated **Attribute** of
> **INT**. Ghoman has an INT of **10**, while Terri has an INT of
> **15**. Upon graduating from the class, both characters have a **Skill level** of
> **Computers 1**, but Ghoman's **Skill score** is **11**, while Terri's is
> **16**. Terri is smarter than her friend; she simply got more out of the experience, despite the fact that they both received the same training.

In addition to the free Skills a character gets for their Background at
rolling-up time, they also get **4 Character Points (CPs)** then, with
which to obtain new Skills. Only 1 CP can be expended per
*new* Skill learned, meaning all new Skills start at Skill level 1. Once the character earns more CPs (that is to say, after a little more adventuring), up to 3 CPs may be pumped into
that Skill at any one time.

Skills are based upon **Attributes**. Each Skill name on the list in
this chapter has one or more Attributes under it in parenthesis. To
determine a character's score in a particular Skill, take the Attribute
number, and add their Skill level to it. Once an associated Attribute is
chosen for a Skill, it cannot be changed.

> **Example**: Fromo has the Skill **Cargo Handling**. Consulting the
> list, we see that **Cargo Handling** can be based on either **STR** or
> **WIS**, at the player's discretion. Fromo's not an especially strong guy,
> so his **Cargo Handling** is based on WIS (he intuits the best way
> to load a ship, rather than move everything around until it fits).
> Fromo's WIS is **12**. 12 plus his Level of 2, equals 14. Fromo has
> **Cargo Handling** 2, with a score of **14**. Whenever asked to do a Skill Check for **Cargo Handling**, if there are no other modifiers, Fromo's player must roll 14 or lower
> on **1d20** in order to succeed.

When choosing a Skill, write the name of it down on your character sheet
under the Skills section. Next to it, write the associated Attribute, as
it is detailed in this chapter. Starting with that number, whatever it
is, add in your Skill Level, even if it's just 1. Write this number
down. This is your Skill score.

**NOTE**: If your GM is using the *Advanced Initiative* rules, you'll
also want to determine your **Initiative Differential**, or ID, for each
Skill. Subtract your Skill score from **20**. That is your ID for that
Skill. Write this number down in the section for it next to the Skill
score on your Character Sheet.

If you're attempting to use a Skill that you *don't* actually possess,
the default score to roll against is that Skill's associated Attribute.
You must roll versus this Attribute score, equal to or less on **1d20**,
in order to succeed. Negative modifiers to your score *always* apply
when using an unfamiliar Skill in this manner. Your GM will determine
what those modifiers are on a case-by-case basis, but the more difficult
the challenge, the greater the penalty.

There are no minimum Attribute scores required for any Skill in this
game, but some have other Skills as prerequisites. You must have the
required Prereqs first, in order to learn the Skill, though if you have
enough Character Points (CPs), you may take both the Prereq(s) and the
desired Skill at the same time.

When first rolling up a character, CPs may only be expended on gaining
new Skills, *not* on improving them. Later on, after earning new
Character Points, CPs may be placed into any Skill desired, or in new
Skills; however, new Skills will *always* start at Level 1, and stay
there until the next time CPs are earned. No one begins as an expert.

Prime Skills (see below) allow a character to start at Level 2 in
something. That is the only way a character may have a Level higher than
1 in any Skill right at the beginning. After this, CPs earned while
gaming may be expended freely upon Skills or Attributes.

<div align="center">

![arriving-scientists-300x465](../Images/arriving-scientists-300x465.gif "There's something unusual about this planet!")\

</div>

***

## Free Skills

A person's Background determines at least some of Skills they have when
they start. To simulate this, each character Background has a list of
free Skills which do not cost any CPs -- the character simply has them,
at Level 1 right from the start. Write these down on your character
sheet. (See **Background**.)

***

## Prime Skill

All characters, except for those with the **Drifter** Background, may
choose one Prime Skill when they start the game. A Prime Skill offers
several benefits:

-   Characters choosing a Prime Skill at rolling-up time begin the game
    at Level 2 in that Skill for free. This is the *only* way to have
    any Skill at Level 2 at the very beginning.
-   When rolling against a Prime Skill, the character is allowed a
    ***Second Chance*** roll, should want it.  This means, whenever a Skill check of some
    kind is called for with the character's Prime Skill, the player may,
    at their option, choose to ignore the result, and immediately
    re-roll the **1d20**. They must, however, accept the second result, whatever it is, even  if it's worse than the first.
-   If **Stunned**, a character may still use their Prime Skill at their
    *normal* Skill score, not at **1/2** their score.

***

## Skill Notes

As a general rule of thumb, most Skills on the list may be performed
adequately ***without rolling any dice***, if they are attempted in
ordinary, non-stressful situations. If there is no great time crunch
involved, or some kind of survival situation going on, repairing a
starship's reaction engine with **Engineering: Main Drive** is assumed
to be automatically successful -- *if* it's possible at all, depending
upon the circumstances. When a character can take their time to do the
job correctly, without a deadline, or without enemies shooting at them,
they'll eventually get it right. The GM may impose exceptions to this
policy at any point, and for any reason (including just for fun).

Modifiers for characters attempting to use Skills which they don't
possess range from **-1** to **-10**, depending upon the Skill, and
circumstances. This is in addition to any situational modifiers the GM
may impose. Again, these rolls would not be actual Skill Checks, since
the characters don't actually have the Skills in question. Rather, these
are Attribute Checks, rolled against the Skills' associated Attributes.
Hopefully, it's understood that negative modifiers can add up quickly.
While you may not be left with much choice sometimes, attempting to use
Skills that you *don't* actually have is always risky.

Below is a current list of Skills for Stardrifter. Included with each
are some possible suggestions for their use, which will hopefully offer
some inspiration.

<div align="center">

![testing-rocks-300x372](../Images/testing-rocks-300x372.gif "The readings are off the charts!")\

</div>

------------------------------------------------------------------------

## Skill List

#### Astrogation
(INT)
Prereq: ***Science***

Being able to discover one's location in space through the use of
navigation software and sensors, and to plot a course for a space vessel
to follow. This includes laying a course via ***starjump***, as well as
through normal space. Many ships use dedicated AIs for this purpose,
both in normal space and for starjump, but all legitimate commercial and
military vessels (though not necessarily privately-owned, or
hired/rented ones) require that someone in the crew also possesses this
Skill, so as to double and triple-check the computer, and to act as a
manual back-up in case of problems. Those hired expressly for this
purpose are usually referred to as *navigators*.

**Possible Uses**: *Setting a course for a boat or ship; reading and
assessing navigation data; estimating the location of other vessels
based on known data; teaching **Astrogation**; assessing space coordinates.*

<br />

#### Athleticism
(STR or DEX, *pick one*)

Being generally accomplished in, or at least knowledgeable about,
sports, athletic pastimes, and exercise. For each level of this Skill, a
character's movement increases by 1; that is, in Combat, this character
moves at 7 meters *plus* whatever Level of ***Athleticism*** they have.
This Skill also has a positive effect on a character's ability to lift
weight; in addition to the normal lifting capacity afforded by STR alone
(10 kgs. per STR point, lifted up to chest height under 1 Terran gee),
characters with ***Athleticism*** may lift an additional 10 kgs. per
Skill Level.

**NOTE**: Martial arts, boxing, or other forms of hand-to-hand fighting
fall under the ***Combat: Hand-to-Hand*** Skill below.

**NOTE**: This Skill can be used for throwing objects like balls, rocks,
or bottles in a fight. Range is 10 meters plus 2 meters per Skill level.

**Possible Uses**: *Lifting heavy objects safely; running quickly or for
a lengthy period; swimming (an uncommon capability in outer space);
climbing; tackling an enemy; getting around obstacles quickly and
efficiently (parkour); talking sports with other fans as a stand-in for
**Social Engineering**.*

<br />

#### Bureaucracy
(WIS)

Knowing what office to apply to, what middle manager to call, what forms
and applications to fill out so as not to be lost in the maze of
policies, laws, and general practices of the various governments,
businesses, and large organizations throughout space. This may
occasionally be combined with, or used along side of, such Skills as
***Social Engineering*** and ***Lying***, but it is considered distinct,
since most bureaucracies have a mountain of laws and rules that simply
*must* be followed, regardless of what individual employees might want
to do to help (or hinder) the character. This Skill can help navigate
these rules and policies successfully.

**Possible Uses**: *Applying for licenses and permits; digging through
public records; digging through military records; understanding
governmental or industrial structures and policies; navigating the
hierarchy of big businesses to find that one person you need to talk to;
navigating through the automated messaging and call systems of large
organizations.*

<br />

#### Cargo Handling
(STR or WIS, *pick one*)

Loading and unloading freight safely from space vessels, stations, or
even on planetary bodies. The character understands how to work
effectively and efficiently; how to use loadbots, pallet jacks, small
cranes, and other tools vital to the process; and how to do all these
things without injuring themselves, others, and any equipment or
property.

**Possible Uses**: *Safely loading or unloading space vessels; working
as a cargo hand on space vessels; working on the docks of a space
station; working at a spaceport on a planet somewhere; finding a
particular crate that's been stored away in a giant warehouse;
understanding general procedures of warehouses and shipping depots.*

<br />

#### Combat: Bladed
(DEX)

Fighting with knives, daggers, small swords (including machetes and the
like), etc.

**NOTE**: This Skill does not expressly include throwing these types of
weapons with any great proficiency.

**Possible Uses**: *Knife fights; sword fights; (literal) backstabbing;
teaching this Skill to others.*

<br />

#### Combat: Bludgeon
(STR or DEX, *pick one*)

Fighting with clubs, pipes, and batons of various sizes, hitting people
with chains, cables, or other objects, etc.

**NOTE**: This Skill does not expressly include throwing these types of
weapons with any great proficiency.

**Possible Uses**: *Rioting; repelling rioters; mugginh people; smashing
bottles over heads in a bar fight; teaching this Skill to others.*

<br />

#### Combat: Dual-Wield
(DEX)

The ability to use a weapon in either hand, and attack with both in the
same round, without penalty. This is a specialized Skill, limited to
weapons which are designed to be used one-handed, such as knives, clubs,
swords, pistols, etc. It goes without saying that both hands must be
free so that each may hold a weapon. This can be a risky maneuver, but
it can also be effective.

A character with this Skill must state they will attempt to use it at
the beginning of the round, before they roll any dice. When it is the
character's turn, they roll versus their ***Combat: Dual-Wield*** Skill
first, before actually rolling to hit. If successful, they may make a
normal roll to hit for each of the two weapons in the same round,
without any penalties. If they fail, it indicates they got fouled up
somewhere; they may choose to *either* make a roll to hit for each
weapon in the same round, but with a -4 penalty on their Skill score for
each, *or* they may make only a single attack with one of the weapons,
but with a -2 penalty on their Skill score.

**Possible Uses**: *Fighting two different opponents at the same time;
intimidating opponents; doubling attack rate; teaching this Skill to
others.*

<br />

#### Combat: Hand-to-Hand
(STR or DEX, *pick one*)

Fighting effectively without a weapon. Player may state their character
uses any martial arts style desired, including just general street
brawling. Damage upon a successful hit is equal to **1d4** plus **1**
point per ***Combat: Hand-to-Hand*** Skill Level. Any successful attack that
does **5** or more HP (not Stamina) of damage, requires the target to
roll a successful **SAVE: Physical** or be Stunned for **1d4** rounds.

**NOTE**: *Combat: Hand-to-Hand* is equivalent to ***Weapon DC: 1***.

**NOTE**: Certain types of Armor protect their wearers from Stun
effects, no matter how much damage the attack inflicts.

**Possible Uses**: *Street fighting; tournament fighting; improvised
fighting; capturing someone alive; assessing the **Combat:
Hand-to-Hand** Skills of others; teaching this Skill to others.*

<br />

#### Combat: Heavy Weapon
(STR or INT, *pick one*)

Being able to use large shoulder-mounted or backpack-carried weapons,
such as human-carried artillery, strap-on charpacs, portable railguns,
S2A/O missile launchers (Surface-To-Air/Orbit), etc. This Skill assumes
a certain familiarity with all these weapon types (which is unrealistic,
but hey, it's just a game).

**NOTE**: Heavy Weapons of any kind are almost universally illegal in
civilian hands. Possible exceptions include registered mercenaries,
civilian weapon suppliers and sales personnel; and members of the Noble
class over in the Empire.

**Possible Uses**: *Battlefield combat; anti-mecha strikes;
ground-to-orbit attacks; attacking space vessels from the surface of a
station; teaching this Skill to others*.

<br />

#### Combat: Pistol
(DEX)

Includes firearms and energy pistols, such as Stunners.

**Possible Uses**: *Dueling at high noon; back-alley shootouts;
assassination; protecting the targets of assassination; assessing the
**Combat: Pistol** Skills of others; teaching this Skill to others.*

<br />

#### Combat: Rifle
(DEX)

Includes firearms and energy weapons.

**Possible Uses**: *Shootouts; hunting; self-defense; robbery,
assassination; assessing the **Combat: Rifle**
Skills of others; teaching this Skill to others.*

<br />

#### Combat: Thrown Object
(STR or DEX, *pick one*)

Allows for accurate thrown attacks with smaller weapons, such as knives,
clubs, bottles, rocks, and the like. At the GM's option, items such as
swords, chairs, trash cans, or nets can also be thrown in combat
(perhaps with modifiers). This Skill may have some crossover with
***Athleticism***, especially with weapons like spears or javelins.

-   Range for smaller objects that have to hit a certain way in order to
    be effective (such as knives) is **10** meters plus **2** meters per Skill level.
-   Range for other types of small objects, such as balls, rocks, or
    mini-grenades, is **30** meters, plus **10** meters per Skill level.
-   Range for larger, heavier objects, such as cinder blocks, bowling
    balls, etc. is **3** meters, plus **1** meter per Skill level.

**Possible Uses**: *Quiet attacks in stealthy situations; bar fights;
sporting events; tossing grapple lines; teaching this Skill to others.*

<br />

#### Computers
(INT)

Computers are ubiquitous in this future, so much so that operational use
of a computerized interface may be considered universal, for all intents
and purposes. Those that learn this Skill, however, can do more than
just operate a computer. The ***Computers*** Skill includes programming
and data analysis, networking, hacking/cracking, and general system
penetration techniques.

**Possible Uses**: *Espionage; malicious digital attacks; protecting
computerized systems from enemy attacks; learning/operating unusual
systems; cracking systems; encryption and decryption.*

<br />

#### Criminality
(INT, WIS, or CHA *pick one*)

This is the Skill of understanding criminal behavior and activities. The
character may have taken courses in law enforcement, or just might have
grown up in a rough neighborhood. Whatever the case, they know how
victims can be conned, pickpocketed, and extorted; how stolen goods can
be fenced (along with the use of ***Social Engineering***, at the GM's
option); how victims can be mugged, murdered, intimidated, or "taught a
lesson"; and how they can have their homes, businesses, and vehicles
successfully broken into. Pairing ***Criminality*** with ***Engineering:
General Repair*** might be useful for cracking safes and bank vaults, or
protecting against such crimes. Some people might start a security
service, and use this Skill to keep their clients safe from criminal
behavior. Others might be criminals, themselves, and use this Skill to
achieve their nefarious goals.

**Possible Uses**: *Protecting against criminals; earning money without
a job (a real one, anyway); working for gangsters; disposing of bodies
starting a criminal gang; being a police officer or investigator; being
a private eye; bodyguarding; spotting criminals in a crowd; identifying
the work of criminals.*

<br />

#### Engineering
(INT or WIS, *pick one*)

In this game, ***Engineering*** covers a very wide spectrum of hands-on mechanical, electrical, and electronic knowledge and capabilities. Space vessels, colony stations, and survival shelters on hostile worlds all require people with the Engineering Skill. It's safe to say that life in space would be impossible without it. People with this Skill can make things, assemble and disassemble machinery and structures, look for signs of tampering or sabotage (or, conversely, *cause* sabotage), and make repairs on machinery and electronic devices.

***Hit Point*** damage to a vessel, from accidents, space combat, or whatever, is restored through the successful application of Engineering. (See the chapter on **Space Combat**).

Very simple operations, like checking engine status, turning simple
machines on and off, operating tools and diagnostic equipment safely,
etc., do not generally require any kind of die roll. Attempting to do
something potentially dangerous, or attempting to do nearly any
Engineering task while the character is in a crisis (such as being shot
at), *will* need a Skill Check, possibly with a penalty of some sort.

Nearly all systems in this future time have diagnostic and monitoring
sensors built in as part of their designs. Installing, fixing, and
maintaining such sensors is part of Engineering training at each level,
as is running any diagnostic checks with this equipment.

Each Skill level reflects an increase in knowledge and experience. The
following table is a guide to the sorts of capabilities that a character
with Engineering can perform at which level, without penalty. Any
difference between a character's Skill level, and the task they are
attempting, represents either a bonus or a penalty to their Skill
Check.

> **Example**: *Indio has **Engineering** 1, for a Skill score of 12.
> She attempts to repair the artificial gravity system of her spaceboat.
> Repair of AG is a Level 4 Capability, which gives her a -3 on her
> score for the purposes of this Skill Check, bringing it down to 9. She
> rolls **1d20**, and gets an 11; normally a success, the -3 makes
> this a failure. The job is too hard for her right now.*
 
| **Skill Level** | **Capabilities** |
| :---:           | :---             |
| 1               | **General Repair/Maintenance** of basic space vessel systems, including mechanical, electrical, electronic, plumbing (including liquid mass stabilizers), HVAC and associated life support, filtration, refueling, kit or unit assembly/installation tasks.         |
| 2               | **Sensor and Communications Repair/Maintenance**, including ship defense controls (Gunnery suites), radar systems, lidar, spectral analyzers, IR sensors, radio broadcast/reception, etc. *Includes fine repair or even construction/design work for all Skill Level 1 Capabilities.* |
| 3               | **Main Drive Repair/Maintenance** of rocket, reaction mass, jet, and ramjet/scramjet engines (large and small), repair/maintenance of hull/superstructure. *Includes fine repair or even construction/design work for all Skill Level 1-2 Capabilities.* |
| 4               | **Artificial Gravity/Inertial Compensator Repair/Maintenance**, as well as hands-on repair/maintenance of weapon systems. *Includes fine repair or even construction/design work for all Skill Level 1-3 Capabilities.* |
| 5               | **Starjump Repair/Maintenance**. *Includes fine repair or even construction/design work for all Skill Level 1-4 Capabilities.* |
| 6               |  **Space Vessel Design**. Designing entire space vessels from scratch. *Includes fine repair or even construction/design work for all Skill Level 1-5 Capabilities.* |

**NOTE**: This Skill has a focus on space vessels, but this knowledge is
applicable to any similar systems, whether upon space stations,
asteroids, planets, or whatever.

**NOTE**: Use of this Skill for anything that calls for a die roll
generally requires the use of specialized tools, or at least a portable
**Tool Kit**. (See the chapter on **Equipment**.) Attempting to use
***Engineering*** without tools, especially when trying to build,
repair, or test something, means a -4 Skill Check penalty, on top of any
other modifiers in place.

**Possible Uses**: *Repairs and maintenance of space vessel and station
vital systems; repair of weapons and other devices; bypassing security
systems; picking electronic and mechanical locks; installation/removal
of electrical and plumbing systems.*

<br />

#### Exosuit
(STR)

Using any one of a wide variety of garments that create an artificial
environment for the wearer. This includes pressuresuits, powered armor,
extreme thermal protective (ETP) suits, fully-contained chemical,
biological, and radiological (CBR) hazmat suits, and more.

**Possible Uses**: *EVAs in space; exploration on the surface of hostile
planets; teaching proper pressuresuit usage; entering biohazard zones;
entering large fires; maintenance of exosuits; selling/dealing in
exosuits; putting on an exosuit quickly in a crisis.*

<div align="center">

![two-people-floating-360x551](../Images/two-people-floating-360x551.gif "Get that patch ready for the station's hull!")\

</div>

#### Explosives
(INT, WIS, or DEX, *pick one*)

(Prereqs: *Engineering* and *Science*)

Safely building, handling, and setting explosive devices, such as
chemical explosives of various kinds; building detonators of various
kinds.

**NOTE**: With the exception of Mini-Grenades, most explosives are
illegal in civilian hands. Possible exceptions include mining and
demolition operations, civilian contractors to law enforcement and
military organizations, and commercial chemical/munition suppliers.

**NOTE**: This Skill does not expressly include throwing these types of
weapons with any great proficiency.

**Possible Uses**: *Setting bombs; defusing bombs; finding hidden bombs;
building or removing IEDs; demolitions; setting booby-traps; teaching
this Skill to others.*

<br />

#### Gunnery
(INT, WIS, or DEX, *pick one*)
(Prereqs: *Computers* and *Engineering*)

Operating a modern civilian-class gunnery suite, which integrates
sensors, computer operations, and a vessel's installed armaments into a
single software interface, or dedicated equipment stack. Includes
troubleshooting and maintenance capabilities.

**Possible Uses**: *Ship defense; piracy; mercenary work; assessing the
**Gunnery** Skills of others; teaching this Skill to others.*

<br />

#### Interrogation
(WIS)

Questioning people, and getting truthful or revealing answers. This
includes interviewing witnesses, conducting job interviews, or giving
criminal suspects the third degree. This can be combined with
***Criminality***, ***Lying*** or pretty-much any other Skill, depending
upon the situation, which can sometimes provide bonuses (GM's
determination).

**Possible Uses**: *Applying or hiring for a job; obtaining information
from people; being a journalist; detecting evasiveness or lies;
conducting interviews; analyzing information obtained through this
Skill; teaching this Skill.*

<br />

#### Lying
(CHA or WIS, *pick one*)

Telling convincing untruths, and having them be believed. Formal
teaching may come from a variety of sources, including acting classes;
law enforcement, military, or intelligence training (especially
regarding interrogation and/or espionage); criminal tutelage (especially
from organized crime operations); and more. Informal training might be
obtained through simple grifting, street hustling, storytelling, or even
just a natural predisposition (being a compulsive liar).

***Lying*** is distinct from ***Social Engineering***, in that, reality
is not much of a barrier to the liar. Rather than schmoozing the doorman
with the names of acquaintances, so as to gain access to a private party
(the way someone with ***Social Engineering*** might approach things), a
character using the ***Lying*** Skill might just say that they *are* one
of these people. If it works, great! If the truth comes out, well,
whatever; maybe you need another lie! ***Lying*** is generally more
adaptable than ***Social Engineering***, but it is usually just a
temporary solution.

**Possible Uses**: *Talking your way out of trouble; talking your way
*into* trouble; conning a mark; making up believable excuses;
salesmanship; negotiations; detecting evasiveness or lies;
intimidation.*

<br />

#### Medico
(INT or WIS, *pick one*)

As a term, **Medico** covers the vast breadth
of medical science in this future time. It's also used to refer to any
practitioner of medicine, regardless of the exact branch, since there
are a dizzying number of degrees, titles, and certifications available
throughout space. In ***Stardrifter***,
especially regarding player characters, the focus of the Medico Skill is
on emergency first aid.

A character with this Skill may attempt to heal **HP** or **Stamina** damage in themselves or in another
person. Upon a successful Skill Check, the character restores **1d4** to
HP, and then to Stamina, in that order: once HP is fully restored, any
healing goes to Stamina. If performed on someone who is at **0** HP or
below, it might be possible to revive them (bring them to at least
**1** HP) with a successful Medico Skill
Check. Even if a successful roll does not revive this person (that is,
does *not* bring them up to at least **1** HP), the person is considered
to be *stabilized*. They will no longer lose HP, and will, in fact,
begin healing HP at the normal rate of **1** point per 24 hours of rest.

If the character with Medico does fail their Skill Check when attempting
to bring this person up to at least **1** HP, they remain by this
person's side continually, in order to maintain their stable state.
Should they leave this person's side without someone else taking over
who *also* has the Medico Skill (or who doesn't, but who makes a
successful WIS Attribute Check with the usual penalties), the victim in
this scenario will begin losing HP again immediately. This is known as
*soft stabilization*.

The attending character may attempt another Medico Skill Check on this
person the next day (*24* hours), in order
to properly heal or stabilize them; that is, bring them to **1** HP, or,
at the least, successfully make their Skill Check this time, so as to
stabilize them in such a way that they may be left on their own for a
while (up to **3** hours per Medico Skill
level), without losing any HP. This is known as *hard
stabilization*. If someone who is hard
stabilized is checked on periodically by a character with Medico, no
further die rolls are required.

Once a character is stabilized, they remain so until they are healed up
to at least **1** HP, unless they take further damage before then. Also,
if one character fails a Medico Skill Check, another character with
Medico may make an attempt. Healing from more than one character with
this Skill cannot be "stacked", that is to say, if one character heals
someone for a certain number of points, they can't then turn that person
over to someone else to try and get more healing, unless
**24** hours have passed. (Also, see the
chapter on **Damage and Healing**.)

**NOTE**: If this Skill is used in a relatively non-stressful situation
(such as in a safe, quiet room, away from the fire fight), then no Skill
Check is required (unless the victim is at
**0** HP or below); the use of the Skill is
considered to be automatically successful. Attempting to use Medico
while the pressure is on, however, *always*
requires a Skill Check.

**NOTE**: A ***Medico Kit***
is usually used in conjunction with this Skill, as it can increase the
character's effective level regarding Skill Checks, along with
increasing the number of healing points restored. (See
**Equipment**.)

**Possible Uses**: *Emergency first aid; identifying drugs or poisons;
concocting drugs or poisons (usually in conjunction with **Science**); diagnosing injuries and common illnesses; inflicting torture; reading medical charts; understanding medical information of a
technical nature.*

<br />

#### Performance
(CHA)

Using a performing art of some sort. Covers dancing, singing, or acting
(including stand-up comedy or storytelling). For simplicity's sake, the
character may roll against this one Skill to attempt any or all
performing arts. This Skill might be able to substitute for ***Lying***
and/or ***Social Engineering***, in certain situations (GM's option).

**Possible Uses**: *Putting on a show; creating a distraction; talking
your way out of trouble; talking your way into trouble; starting or
joining a performing group; assessing the **Performance** Skill of
others; detecting lies; teaching this Skill to others.*

<br />

#### Piloting: Air
(DEX or WIS, *pick one*)

Operating heavier-than-air vehicles for in-atmosphere flight. (Includes
surface-to-orbit shuttles.)

**Possible Uses**: *Flying such vessels as fixed wing aircraft, tubofan
aircars, rotary aircraft, etc.; basic maintenance of aircraft; assessing
the **Piloting: Air** Skill of others; teaching this Skill to others.*

<br />

#### Piloting: Ground
(DEX or WIS, *pick one*)

Operating cars, trucks, buses, rollers, bicycles, motorcycles, and small
trams.

**Possible Uses**: *Driving on developed roads; driving upon dirt roads,
or over no road; high-speed chases; fast getaways; assessing the
**Piloting: Ground** Skills of others; teaching this Skill to others.*

<br />

#### Piloting: Space
(DEX or WIS, *pick one*)

Operating spaceboats of most designs, and starships when traveling through normal space. Larger space vessels require a higher level of this Skill. (See the chapter on ***Space Vessels***.)

**Possible Uses**: *Hiring on as a ship's pilot; outrunning pirates;
dodging missiles; assessing the **Piloting: Space** Skills of others;
teaching this Skill to others.*

<br />

#### Piloting: Water
(DEX or WIS, *pick one*)

Operating powered vehicles for water travel, such as boats and
hovercraft. (Includes small
submersibles.)

**Possible Uses**: *Moving people or cargo across a body of water (or
liquid methane, lava, mercury, gas giant atmosphere density terminator,
etc.); teaching this Skill to others.*

<div align="center">

![take-off-400x289](../Images/take-off-400x289.gif "Here we go!")\

</div>

#### Science
(INT)

There are a dizzying number of scientific branches in this future time.
The Science Skill represents generalist training in the scientific
method, and in broad branches of science and research, including (but
not limited to) biology, genetics, physics, astronomy, chemistry, and
materials science. Characters with this Skill have both theoretical and
practical training to some degree; they can assist scientists, and even
conduct simple experiments of their own.

**NOTE**: Conducting field tests or research generally requires a
**Science Kit** (see the chapter on ***Equipment***)

**Possible Uses**: *Understanding/using scientific equipment; conducting
experiments; creating chemicals/machinery/applications; understanding
scientific reports; creating scientific reports based on collected data;
collecting scientific data; interfering with the scientific or
engineering efforts of others.*

<br />

#### Social Engineering
(CHA)

It's not what you know, it's who you know, and how you play them. While
there might be a bit of crossover here with ***Lying*** or
***Performance***, this is
considered to be a distinct Skill all its own. Finessing a situation
isn't the same as fabricating the truth or assuming a different persona.
A business person may use ***Social Engineering*** to make a sale; they
can call upon, or refer to, friends and acquaintances in the industry
for leads; they can introduce people to each other; and they can find
out what their client likes and then try to provide it.

**Possible Uses**: *Learning who you need to talk to, then finding a way
to talk to them; salesmanship; arbitration; interrogation; negotiation;
diplomacy; management/command; making friends; teaching; talking your
way past a human obstacle like a doorman.

<br />

#### Stealthiness
(DEX)

Moving quietly and unseen, or just hiding in plain sight. This includes
using the cover of darkness to remain hidden, watching someone on a busy
street, or hunting out in the wild.

**NOTE**: This cannot be performed while the character is in an exosuit
or powered armor.

**Possible Uses**: *Stalking/shadowing someone; avoiding a stalker;
hiding in shadows; hiding in plain sight; general surveillance;
burglary/theft; assassination; combat; spying; sneaking around.*

<br />

#### Stewarding
(DEX or WIS, pick one)

This provides the character with a generalized knowledge of the many
service-related techniques and equipment associated with caring for
waking passengers upon space vessels. It can sometimes be directly
applied to other types of work, such as cleaning services, food service,
sanitation, or more.

**Possible Uses**: *Hiring on as a steward for a space vessel; waitstaff
in a restaurant; hotel management; cleaning services; butler or maid
services; housekeeping; basic food service.*

<br />

#### Survival: Hostile Environment
(INT or STR, *pick one*)
(Prereq: ***Exosuit*** and ***Survival: Nature***)

Being able to survive in extreme environmental conditions. This includes
planetary or lunar surfaces with very high or low surface temperatures
and/or atmospheric pressures, poisonous or caustic atmospheres, unstable
ground, excessive vulcanism, high radiation, meteoric bombardment,
continual storms, etc.

**Possible Uses**: *Working on terraforming projects; rescuing people on
dangerous worlds; working under extreme conditions on Terra or other
"tamed" worlds; surviving in extreme work environments, such as
super-cold freezers, reactor cores, etc.*

<br />

#### Survival: Nature
(INT or STR, *pick one*)

Surviving, or even thriving in the great outdoors, upon Terran-style
planetary surfaces. This includes foraging for food, finding shelter,
locating potable water, tracking prey; basic hunting and fishing (no big
game); surviving weather extremes.

**Possible Uses**: *Homesteading; finding lost hikers; tracking people
through natural environments; avoiding being tracked by others,
military-style deployment or training in the field.*

<br />

#### Survival: Urban
(INT or CHA, *pick one*)

Surviving homelessness in planetary-based cities and upon space
stations, and ship-hopping upon large space vessels (space hobos). This
includes locating shelter, food, water, medicine, and small amounts of
money.

**Possible Uses**: *Living on the cheap; hiding from people; knowing
where important buildings or facilities are located in a city-like
environment, panhandling; avoiding police; avoiding gangs or criminals.*

<br />

#### Survival: Vacuum
(INT)
(Prereq: ***Exosuit***)

Surviving vacuum conditions, wearing a pressuresuit of some kind. This
Skill covers equipment such as chemical extractors for obtaining oxygen
and hydrogen (for air and water) from the rocks and other natural
materials found on asteroids and planets in vacuum. How to use,
maintain, and improvise CO<sup>2</sup> scrubbers.
How to use and improvise biowaste solutions. Includes the ability to
maintain and repair pressuresuits and emergency vac shelters.

**Possible Uses**: *Homesteading on asteroids; rescuing people who are
in vacuum; improvising an emergency pressuresuit or vacuum shelter
(perhaps in conjunction with **Engineering**); working in open space.*

<div align="center">

![man-in-a-mask-400x320](../Images/man-in-a-mask-400x320.gif "This looks like a good spot to settle down!")\

</div>
