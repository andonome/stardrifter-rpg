# Money

## The *Q*

The standard monetary unit for much of the settled galaxy is an
electronic currency call the **Credit Unit of Exchange**, or C.U.E.,
universally shortened to just ***Q***.

***Q*** exist as data in a computer network, or can be transferred to
specialized data storage units called wallets. Despite the name, wallets
may take nearly any form, from a data stick, to something installed
under the skin in a cybernetic implant. Some wallets are encrypted, only
allowing their owner to make use of them, while others are more
universal, allowing use by anyone who possesses them.

All characters start the game with
2500***Q***

With this money, they may buy equipment, weapons, and armor. They
probably won't want to spend it all before the first game, since there
are other expenses to be met here and there, including *Cost of
Living*.

<div align="center">

![robot-helper-200x339](../Images/robot-helper-200x339.gif "CLICK-May-I-get-you-anything-else-sir?-CLICK")\

</div>

## Cost of Living
In reality, the cost of living
(***CoL***), from one star
system to another, or even within the same system, tends to vary widely.
This does not include any special purchases, such as weapons or other
equipment; this is only a reflection of how much it costs to find basic
lodgings and food for one **24** hour day in
this place.

Going to a nice restaurant, paying application fees, bribing people,
large or extravagant transportation expenses, or other sundry costs of
the adventuring life, are *not* covered by the CoL charge. Decent meals,
lodging, and local transportation in and around the neighborhood *are*
covered.

As an aspect of practical game-related book keeping, and depending upon
the GM, characters must either expend this money at the start of the day
(so no one forgets to do so), or at the end of the day, after these
expenses will have been made.

To simplify things in
***Stardrifter***, the CoL
is broken down into just three categories: **Low**, **Medium**, and
**High**, based upon the relative quality of a place, or the
quality-of-life expected there. The socioeconomic situation of most
places, whether space stations or planets, will generally reflect this
CoL, which in turn, reflects the economic opportunities available. By
and large, *most* places in the galaxy with any sort of economy fall
into the Medium category, at least, as far as adventuring PCs are
concerned. A few places have exorbitant CoLs, while frontier worlds or
abandoned stations won't have any at all (but no available services,
either).

### CoL Per Day, By Quality of Location
-   1***Q***: **Low**
-   5***Q***: **Medium**
-   10***Q***: **High**

Depending upon the place, vagrancy might or might not be illegal. Either
way, it is possible for someone with the ***Survival: Urban*** Skill to
ignore the CoL, provided the place is sufficiently industrialized. They
may do this without any need for a Skill Check, unless or until the GM
determines that a stressful situation has arisen (being harried by
police or criminals, etc.). Terraformed planets with rural or wild
locations can allow those with ***Survival: Nature*** to do the same.

<br />

## Docking Fees

If the characters own, or are responsible for, a space vessel docked at
a station, the fees associated with dock rentals, maintenance costs,
atmo recharges, and minor repairs are equal to **the above-listed fees
in *Q*, plus the vessel's Size times 100**, per 24 hour day. (See
the chapter on Space Vessels for information on vessel sizes.)

> Number of ***Q*** for CoL + Size x 100

This money is in addition to the CoL for each individual, but does not
include the cost of replacing any missiles fired, Medico supplies, or
food stores. It *does* include potable
water, and biowaste removal.

Missiles are sold in pre-packed cylinders, called, appropriately
enough, **Missile Packs**. Missile Packs cost
**1d6x1000** in ***Q*** to replace (prices do
fluctuate), and the dealer cannot just replace individual missiles
fired, but rather, the entire Missile Pack must be replaced as a single
unit.

Space vessels that are docked do not earn money, they *cost* money, and
it really adds up fast.

> **Example**: *An average Merchanter of Size 2, docked at a Medium
> quality space station, would cost its owner(s) 700**Q** per day in
> docking fees. (5**Q**+2x100). Ouch!*
