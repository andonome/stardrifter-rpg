# Combat

First off, it must be stated emphatically that **combat is dangerous**.
You should do everything you can to avoid it. Character progression in
this game is *not* dependent upon combat, in and of itself, but rather
upon achieving goals, and good role-playing, among other things. (See the
chapter on **Progressing In The Game**).

If you can't avoid combat, then you *really* want to own some Armor.
Armor is more important than weapons when it comes to surviving a fight
in ***Stardrifter***, and it
will likely be your most important possession. Combat, itself, is
described below, but when you're buying **Equipment**, remember to
spend at least some of your ***Q*** on Armor. You won't regret it.

Combat is divided into **rounds**. These rounds are stylized and
admittedly unrealistic tools used to organize the chaos of battle.
Rounds don't correspond to any standard unit of time, but rather, what,
exactly is being done by all the parties involved in the fight. From a
practical standpoint, it's rare to need to know how much actual time is
passing during the fight. Should that become important (*e.g*, getting
past the bad guys to defuse a bomb, etc.) the GM will give a ruling.

All players with characters in a fight perform their actions in
sequence, which is, itself, determined by the Initiative order.
Initiative in combat is determined with either the Basic form, or the
Advanced form.

<div align="center">

![winner-of-fight-428x337](../Images/winner-of-fight-428x337.gif "What did you say abut my mother?!")\

</div>

***

## Initiative
Initiative determines who goes when in the middle of combat or some
other crisis. There are two types of Initiative available in the game:
**Basic** and **Advanced**. Your GM will tell you which one your game is
using.

<br />

##### Basic Initiative
Each round, someone on the PCs' side rolls **1d20**, and the GM rolls
**1d20** for the NPCs. The lowest roll goes first. Re-roll any ties. All
characters on the winning side attack, going in any order they or the GM
likes, followed by the characters on the other side. Despite this
ordering of the combatants, all attacks in a single round are
considered to be happening more-or-less simultaneously. The dice are
rolled simply to aid in playing through the action, and to help
understand what happens.

The Basic Initiative system isn't complicated or precise, nor is it
especially realistic, but it's *more* realistic than breaking every
character's actions down into distinct sequential order. Put simply, it
somewhat simulates the frenetic madness of real combat. It also has the
advantage of speeding up the fight.

<br />

##### Advanced Initiative
If a more abstract approach is desired, either as a matter of general
preference, or because a particular situation requires granularity, the
following optional method may be used. It uses the ***Initiative
Differential*** mechanic. If a GM chooses not to use these Advanced
Initiative rules, then Initiative Differential determination can be
skipped entirely, speeding up the character creation process.

This optional system is used as follows:

-   All players state what actions they intend for their characters.
-   All players roll **1d20** and then add in the ***Initiative
    Differential*** being used (see below).
-   The GM rolls **1d20**+10 for most NPCs, and fits them into the
    Initiative order.

The Advanced Initiative system has the advantage of precision, but it
lacks realism, and can slow combat down.

<br />

**Initiative Differential** (ID)

As has been touched on in their various chapters, Attribute and Skill
scores also come with ID scores.

Finding the ID of a statistic is quite straightforward, and is done the
same for each of them:

-   Start with the number **20**.
-   Subtract the statistic score from that.
-   The resulting number (whether positive or negative), is the ID for
    that statistic.
-   Write each of these IDs next to their statistics, in the spaces
    provided on your Character Sheet.

ID is applied in the game whenever an Initiative roll is required. The
player rolls **1d20**, and then *adds* in their ID score (the GM will
determine which one is to be used, based on the situation at hand, and
the player's intentions). The final number is the character's modified
Initiative roll. The final rolls of all the combatants are compared, and
*lowest* number goes first in the round; then the next lowest, then the
next, and so on.

The purpose of ID is to allow a character's scores to influence the
order of action in a fight or crisis situation where understanding
*what* happens *when* is vitally important to the outcome.
Characters with high DEX, for instance, could have a reasonable chance
of acting more quickly than do others when a combat round begins. Other
statistics can be used in a similar way.

> **Example**: *Rouden has a DEX of 13. **20 - 13 = 7**. Her DEX ID is 7. A
> fight breaks out, and the GM calls for Initiative. Rouden's player
> says she's going to try and to climb a fence to get away before she
> gets shot. Unfortunately, Rouden does not have the **Athleticism**
> Skill, which is what is needed here. The GM states she must use DEX,
> instead. Rouden's player then rolls **1d20**, and gets a 9. **9 + 7 = 16**. The final
> Initiative rolls of all the participants in the fight are compared,
> and it looks like Rouden will be going second in the round. Remember,
> this is merely to determine who goes when; Roudin will have to roll
> against DEX again when it's her turn to see if she even gets over the
> fence at all. This is kind-of bad news, since an NPC that is aiming a
> rifle at her is going first!*

> **Example**: *Rouden has the **Piloting: Space** Skill at level 2,
> with a score of 15. She is sitting in the cockpit of a boat docked at
> a space station. She's been in there a while, and has all the vessel's
> systems warmed up, and online. Unfortunately, it's not actually her
> boat: she and her partners are attempting to steal it, while local
> authorities are attempting to stop them! So long as it's physically
> docked here, the boat's controls can be overridden. The police have a
> handheld device which, if plugged into a dataport that is located
> within a glass-walled control booth across the dock, can completely
> disable the vessel. While Rouden is attempting to disengage the boat
> and fly off, she spies an officer with this device dashing across the
> floor of the control booth. The GM calls for Initiative. What happens
> next can only be determined by knowing which of these two characters
> gets to go first. Being an NPC, the officer will just use her DEX ID,
> which is 10. Rouden, on the other hand, can use her **Piloting:
> Space** ID in this circumstance. Again, her **Piloting: Space 2**
> is 15. **20 - 15 = 5**, so her **Piloting: Space** ID is 5. She
> rolls for Initiative and gets a 12, then applies her ID, for a final
> score of 17. The police officer rolls **1d20** and also gets gets a
> 12. She applies her DEX ID, which is 10, for a final score of 22. Even
> though they both rolled the same number on the die, Rouden's modified
> 17 beats the officer's modified 22, and the boat pulls away before it
> can be disabled.*

Regardless of the Initiative method used, characters and NPCs can do the
following, in any order, during their turn within the round:

-   **Move** up to 7 meters.
-   **Prepare** a weapon or other piece of equipment, or perform some
    fast action with them. (*E.g.*, draw a weapon, reload a weapon, call
    for help on a radio, etc.)
-   **Attack** or use another Skill or Attribute.
-   Talk to each other.

Characters may choose to do one, two, or all three of these things if
they wish, or none at all. Save Checks happen during the round whenever
required, but it is up to the GM to arbitrate what can and can't
reasonably happen in the middle of a fight.

<div align="center">

![smiling-man-with-raygun-350x493](../Images/smiling-man-with-raygun-350x493.gif "Move, and I'll blast you!")\

</div>

<br />

***

## Fighting

To attack an opponent, the player performs a Skill Check by rolling
**1d20** and comparing the result to the ***Combat*** Skill score
they're using in the fight. If the character is using a Combat Skill
they are *not* trained in, or has no Combat Skill of any kind, they will
be performing an Attribute Check, likely with penalties. A successful
roll indicates a hit.

Damage points are subtracted from the opponent's **STAM** first, until
these points are gone, then they are subtracted from their **HP**.

Player characters that are hit roll a SAVE: Physical to take **1/2
damage** (minimum **1** point) for each attack
that reduces **HP**, *not* STAM. By and large, only player characters can
do this, as most NPCs do not have Stamina scores; but even if they do,
they take regular damage, not half, on any HP hits. (It's good to be the
hero of the story.) Player characters with the **Xmil** Background roll
a SAVE: Physical for **1/4 damage**, instead of **1/2** (again, minimum **1** point).

> **Example**: *Deeja is a player character involved in a firefight against multiple assailants. She has **HP: 10**, **STAM: 12**, and a **SAVE: Physical** of **13**. She gets hit twice the first round,
> taking **3** points from one attack, and then **6** more from another, for a total of **9** points off her STAM. She has **3** Stamina left. She fires back and hits one or two of them, but they're still there, and still shooting. Next round, she's hit for **4** points. That takes care of all her Stamina, with **1** point that comes off her HP. Since the minimum damage per successful attack is **1**, her player doesn't roll a **SAVE: Physical** for **1/2 damage**. She shoots back and takes out one of the assailants, but there are still a few more, and they have bullets to spare. Next round she gets hit
> twice: once for **3** points, once for **4** points. This is now coming off HP, so Deeja's player gets a **SAVE: Physical**, to see if **1/2 damage** for these attacks is possible. They are rolled for separately. (If Deeja had the Xmil Background, she'd be rolling for **1/4 damage**, but she doesn't, so she isn't.) The player rolls **1d20**, and gets a **10**. That's successful, so the first attack doesn't do *3* HP, but only **1.5** HP, rounded up to **2**. The player then rolls a **SAVE: Physical** for the second attack, getting a **6**. That's also a successful Save, which reduces the **4** HP of damage to **2** HP. This round, Deeja takes a total of **4** HP of damage from the two hits, instead of **7**. Including the previous hit, she is now left with **5** HP. This fight isn't going well, but it would already be over if Deeja was an NPC.*

<br />

## Civilian versus Military
In this future time, there is a huge disparity between the lethality of
civilian class weaponry and that of military class weaponry. It is
illegal in many parts of space for non-military personnel to own
military class weapons; but even where this restriction is in place,
there are often many exceptions. Military bodies also issue weapons such
as knives and clubs to their personnel, which do the same damage as
their civilian counterparts. Though militaries use them, they are
considered civilian class weapons, since it's okay for civilians to own
them.

<br />

## Combat Damage
All successful attacks either do physical damage to your opponent, Stun
them (see below), or render them unconscious. Damage is the most common
result of most styles of attack, and the amount of damage done is
dependent upon the exact weapon being used.

All Physical damage comes off a character's *STAM* until it is gone (if
they have a Stamina score at all -- most NPCs do not), and then it
starts coming off their *HP*. (See the chapter on **Damage and
Healing**.)

<br />

## Stun
Certain types of combat and weaponry can cause a Stun effect, either in
addition to, or rather than, a loss of STAM or HP. Stunned characters
are not helpless, but they are faced with several immediate, and rather
severe, disadvantages:

-   Movement, as well as Attribute, Skill, and Save Checks, are all at
    *1/2* normal for that character, except for Prime Skills (see NOTE,
    below).
-   If using **Basic Initiative**, any side that has even *one* Stunned
    character on it automatically loses Initiative. If both sides have
    Stunned characters, Initiative is rolled as usual.
-   If using **Advance Initiative**, any Stunned characters go last in
    the Initiative order.
-   Any Stunned character affected by a second Stun before the first one
    wears off, immediately falls unconscious for one hour. Remember,
    too, that the second **SAVE: Physical** is being done at ***1/2***
    normal, because of the first Stun effect.
-   Any rolls to hit Stunned characters in Combat are done with a bonus
    of **+2** for the attacker.

**NOTE**: A Stunned characters may roll Skill Checks versus their **Prime Skill** at their
*normal* Skill score, instead of at **1/2** their Skill score.

<br />

<div align="center">

![space-armor-dark-bw-300x495](../Images/space-armor-dark-bw-300x495.gif "Ready for action!")\

</div>

## Armor
Armor is classified by **Protective Rating** (PR), and by **Structural
Rating** (SR). PR refers to the amount of damage per attack that the
armor protects its wearer from, while SR refers to the total amount of
damage the Armor can take before it is ruined, and must be discarded or
repaired. Players are required to keep track of the Structural Rating of
their character's Armor, in its place on the Character Sheet.

> **Example**: *A character named Aperi is wearing a **flak vest**,
> which is PR-2, SR-20. She gets shot for 4 points of damage, but only
> takes 2 points (in this case, to her STAM, which is 9), because the
> vest absorbed the other 2. Absorbing those 2 points causes damage to
> the vest, and brings it down to SR-18. Later on, Aperi is caught in an
> explosion, taking 6 points of damage. This time, she takes 4 points of
> damage to her STAM, with her flak vest once again absorbing 2. It is
> now at SR-16. Aperi has taken a total of 6 points of damage to her
> STAM. Without the vest, she'd have taken 10, putting her into her HP
> by this point.*

Some Armor can be combined with others, such as a **Ballistic Shield**.
This offers the combined PR of all the Armor types worn. Individual SR
must still be tracked for each piece of Armor, being divided equally
among them (or in any way that the GM rules). Some Armor incurs DEX
penalties, due to awkwardness, and these modifiers are cumulative with
any others.

Characters with certain Skills (***Engineering***, perhaps?) may be able
to fix damaged Armor, at the GM's discretion.

<br />

## Damage Category (DC)

All weapons or attacks are assigned a **Damage Category**, expressed in numbers. The higher
the number, the better. Certain types of Armor or cover are able to
protect against certain DCs. Attacks that have a lower Weapon DC than
what the Armor is rated for (as listed in the description for that
Armor) automatically fail to hit, and do no damage at all to the Armor
itself (that is, no points come off the Armor's **SR** rating). Such attacks simply can't get
past the protection afforded by this Armor, or by the cover from fire.

> **Example**: *Kress has a baseball bat in hand, which the GM determines is equal to **Club (large)**, as listed in the chapter on **Equipment**. It has **Weapon DC-2**. A pirate has just boarded Kress' boat, intent on taking it. Kress attacks with his bat, which would normally be a fearsome assault. Unfortunately, the Pirate is wearing **Powered Armor**. According to the description for Powered Armor, it renders its wearer immune to attacks of **DC-2** or below. The Pirate is effectively a walking tank, while Kress only has a stick. He cannot harm the intruder with that weapon at all, nor can he damage the
> Armor, because every attack just bounces off harmlessly. Kress is about to lose his boat, and possibly his life!*

Some types of Armor do not have a **Weapon DC** listed, meaning that, while they do
offer protection to their wearers, *any* type of weapon attack has a chance to get through.
