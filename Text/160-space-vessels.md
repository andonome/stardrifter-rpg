# Space Vessels

Space vessels come in all shapes and sizes. They are used for a wide
variety of purposes, and are, by definition, found everywhere that
people are. Some have starjump engines installed, allowing travel
between stellar systems. Most do not. Some are gigantic box haulers
capable of moving a warehouse of merchandise at one time; some are just
tiny drones, the size of a person's fist.

<div align="center">

![rocket-orbiting-planet-339x600](../Images/rocket-orbiting-planet-339x600.gif "Prepare to break orbit!")\

</div>

## Designation 
In the ***Stardrifter*** universe, space vessels are designated as follows:

-   **Ships**: Any space vessel possessing a starjump engine, which also
    has a human crew or passengers.
-   **Boats**: Any space vessel which does not possess a starjump
    engine, but which does have a human crew or passengers.
-   **Craft**: Any space vessel which is not designed to have a human
    crew or passengers. Craft which do not possess starjump engines are
    known as *spacecraft*; those that do possess them are known as
    *jumpcraft*. Remotely operated craft are sometimes called
    *spacedrones* and *jumpdrones*, respectively, though other terms
    exist.

Statistics in the game for space vessels and other vehicles share
certain commonalities with those used for characters, including
**HP**, **Movement**, and **Armor**. Vehicles or other machines,
even ones owned by PCs, do *not* have **Stamina** scores, nor do they
get **Save Checks** of any kind.

A truly sapient **Artificial Intelligence** (AI), installed on a space
vessel, may be allowed **Save Checks** at the GM's discretion. If so,
the AI rolls **SAVE: Mental** for anything to do with software, and **SAVE:
Physical** for anything to do with hardware. These Save Checks, if allowed, do *not* include,
nor extend to, the rest of the vessel.

A **Space Vessel Sheet** is provided at the end of the book, which may
be copied out and used. If you are creating a space vessel from scratch,
fill in the statistics on that Sheet as you go through this chapter.

<br />

<div align="center">

![rocketship-300x122](../Images/rocketship-300x122.gif "All systems nominal!")\

</div>

## Space Vessel Characteristics 
The following represents the characteristics associated with space
vessels, as listed on the **Space Vessel Sheet**, at the back of the
book.

<br />

**Name**: The name of a vessel can be almost anything, from the poetic
(*Fields of Lavender*, for a garbage scow) to the utilitarian (*SolCo
Cargo 33*, for a corporate-owned box hauler).

<br />

**Type**: A vessel will be either a ***Spaceship*** (or just *ship*),
with a crew and starjump capability; a ***Spaceboat*** (or just *boat*),
with a crew but *without* starjump; or a ***Spacecraft*** (or just
*craft*), for a vessel without a crew.

<br />

**Size**: This is a reference to the general mass of a vessel, in metric
tonnes. Flying a vessel requires the ***Piloting: Space*** Skill, but
larger vessels are harder to control, and demand greater mastery. For
the sake of simplicity, Sizes come in three categories:

-   **Size 1**: Anything up to 1000 tonnes. (Requires ***Piloting:
    Space*** 1 or better.)
-   **Size 2**: Anything between 1000 and 100,000 tonnes. (Requires
    ***Piloting: Space*** 2 or better.)
-   **Size 3**: Anything between 100,000 and 1,000,000 tonnes. (Requires
    ***Piloting: Space*** 3 or better.)

Attempting to fly a vessel that requires a higher level of ***Piloting:
Space*** than the character possesses incurs an automatic penalty equal to the
differential between what is required and what they have, in addition to
any other situational modifiers the GM may impose.

> **Example**: *Yuri, who only has **Piloting: Space** 1, tries to fly a huge modular
> hauler across a star system. This ship is **Size 3**, at 700,000
> tonnes. **Size 3** vessels require at least ***Piloting: Space*** 3 to
> fly. The difference between this requirement and what Yuri has is 2,
> which is applied to his Skill Check as a -2 penalty. Yuri's
> **Piloting: Space** score is 15. With the -2, it's a 13. Yuri's player
> rolls **1d20**, and gets an 11. Success!*

Attempting to fly a vessel with a substitute Attribute garners an
Attribute Check penalty equal to *double* the Size category of the
vessel, in addition to any other modifiers.

> **Example**: *Sjurinna takes over for Yuri when he has to run to the
> fresher. This is probably a bad idea, since she doesn't have the **Piloting: Space** Skill at all, and has only been watching Yuri fly this thing for the last few minutes. (If Sjurinna had the
> **Drifter** Background, this might all play out differently. But, she doesn't, so it doesn't.) With the hauler being a **Size 3** vessel, she gets a -6 penalty right off the bat
> on her Attribute Check to try and fly it. The associated Attributes
> for **Piloting: Space** are DEX and WIS. At **14**, Sjurinna's WIS is
> better than her DEX, so the GM lets her player choose that for the
> Attribute Check. With the -6 penalty, her 14 becomes an 8, but we're
> not done yet: the GM assigns an additional -2 as a situational
> modifier, because the ship is coming to a heavily trafficked area of
> the star system. Sjurinna's player now has to roll 6 or less on
> **1d20** to pull this off. Not impossible, but Yuri probably
> shouldn't have had all those tacos for lunch!*

<br />

**Rating**: This is a reflection of the over-all quality and technical
advancement of the vessel. Ratings run from **0** (nonfunctional) to
**10** (highest quality, and in perfect shape). Rating covers many
different aspects of a vessel, as described in this chapter, including
such things as sensors. (Sensors are described under
*Detection*, in the chapter on **Space Combat**. Maintenance costs are described in the chapter on **Money**, under *Cost of Living*.)

<br />

**Movement**: This is a reflection of the maximum movement in real
space, expressed in **gees** (G-forces). Higher numbers than a human can
survive are entirely possible here, since nearly all vessels designed to
have human crews also have inertial compensaters installed. Movement
goes from **0** (no self-propelled movement possible) to **20**.
Anything above **10** is a special case, and usually only craft such as
missiles ever go above **15**. Standard safe cruising speeds in most
settled systems range from **1** to *6* gees, depending upon local traffic and
circumstances.

Maximum normal (that is, non-emergency) Movement of a vessel is equal to
it's Rating *minus* its Size category, expressed in gees (minimum 1). A
vessel may run at *double* this (or part thereof) in emergency
situations for a number of hours equal to its **Rating**, before its main drive must be
throttled back to normal. After this, it cannot run above its normal
speed again until it has had a full engineering strip-down and refit.

> **Example** *A brand new average merchanter, cruising out of the
> shipyard, might be of Rating 6 and Size 2. When traveling through
> normal space, its maximum normal Movement would be 4 gees. In an
> emergency, it can travel at 8 gees for up to 6 hours, before it must
> be throttled back to no more than 4 gees again.*

<div align="center">

![rocket-heading-to-planet-400x261](../Images/rocket-heading-to-planet-400x261.gif "Let's see what kind of reception we get!")\

</div>

**Starjump Range**: A reflection of how far a Ship (or Jumpcraft) can
travel in a single starjump, expressed in light years. A Ship's Starjump
Range is equal to it's Rating times **2**. Despite the exact distances
traveled, no ship may make more than **1/2** its Rating in consecutive starjumps (rounded up), before requiring a complete shut-down and servicing by the engineering team. This takes a
number of hours equal to the vessel's Rating for *each* jump that was
made (a vessel with a higher Rating is more complex, and requires more
time to repair or maintain).

> **Example**: *That same average merchanter, at Rating 6, is able to
> starjump to a maximum distance of 12 light years at one time. It may
> do this no more than 3 times in a row before requiring servicing by
> the engineering team, and this servicing for 3 starjumps will take
> them 18 hours to perform.*

<br />

**HP**: This is a reflection of how much damage a vessel can take before
it is destroyed, or at least, becomes nonfunctional. A vessel's HP is
equal to its current Rating score, plus Size category, times **500**. A
vessel loses mobility and any weapon systems once it is reduced to 10%
of its undamaged HP score. At zero HP, the vessel is destroyed, and may
blow up, crack apart, or just drift off into the black, cold and dead.

> **Example**: *That same average merchanter, at Rating 6, Size 2, would
> have 4000 HP.*

> **Example**: *That same average merchanter, after being attacked by
> pirates, is now reduced to 400 HP. It's main drive fails, and its
> weapons are down. Life support is still functional, but maybe not for
> long, should the pirates attack again on their next pass.*

<br />

**Armor PR**: This is a reflection of how much damage a ship's armor can absorb per hit, versus how much the vessel itself takes. A vessel's Armor PR is equal to its Rating plus its Size score, times **10**. (After-market Armor, which can greatly alter this number, may be added on to a vessel
at a later time, at the GM's discretion.)

> **Example**: *That same average merchanter, when new, at Rating 6,
> Size 2, would have had an Armor PR of 80.*

<br />

**Armor SR**: This is a reflection of how much damage a vessel's armor
can take, before it is rendered useless, and must be repaired or
replaced. Once the Armor SR is reached or exceeded, any further damage
goes directly off the vessel's HP. A vessel's Armor PR is equal to its
Rating score times **100**. (Again, after-market Armor can greatly alter this number.)

> **Example**: *That same average merchanter, at Rating 6, would have an
> Armor SR of 300 when new.*

<br />

**Weapons** (DEW, Missiles, PD): In most parts of space, it is legal for
a vessel to be armed for the sake of self-defense. Each vessel can have
a number of weapon systems installed equal to its Rating divided by two
(rounded up). All weapon systems may be fired once each per round, until
ammunition/ordinance is exhausted. DEWs effectively have unlimited ammo,
but missile and gun systems have very hard limits. (See the chapter on
**Space Combat** for details on the different weapon systems.)

> ***Directed Energy Weapons*** (DEW): Styles of DEWs include lasers,
> masers, particle cannons, lantern guns, plasma lances, and more. The
> exact style doesn't matter for the purposes of these rules. Damage in
> combat delivered by the system in question is equal to **3d100** times
> the attacking vessel's Rating score in HP to the target. DEWs draw
> energy from the vessel's engines, and therefore have unlimited
> *ammunition*, so long as the engines are online.

> ***Missiles***: There are many types of missiles available, but for
> simplicity's sake, this rule book only covers basic civilian class
> ship-to-ship missiles. Civilian class attacks from this system are
> fired from a cylinder with ***six missiles***, called a **Missile
> Pack**. Each such attack lets loose the entire missile pack at once.

> ***Point Defense*** (PD): This is a close-in civilian class defensive
> system specifically designed to shoot down incoming civvie missiles
> and small enemy vessels, at relatively close range. This range is
> equal to the defending vessel's Rating times **100** in kilometers.
> There are limitations and special conditions for PD (again, see **Space
> Combat** for details).

<br />

**Cargo Space**: The cargo space available on a vessel depends upon what
sort of vessel it is. As a rule of thumb, space vehicles that have any
cargo space to speak of (and many don't), have their Rating + Size × 10
in metric tonnes available for supplies and freight. Anything
specifically designated as a *cargo vessel*, however, has 80% of its Size
in metric tonnes of cargo space available.

<br />

**Passengers**: Passengers can travel through space on starships or boats in the normal fashion, with a sleeping berth of some kind (usually a cabin), or they can travel in suspended animation. The former are said to be *waking*, and the latter, *frozen*. 

> **Passengers** (waking max): As a general rule, vessels that have room
> for waking Passengers (and many don't) can carry a maximum of Rating
> times **2** passengers, who are not frozen down in suspended animation. Personnel shuttles can carry a maximum of Rating times **10** waking passengers. Any vessel specifically designed as a
> *passenger liner* can carry up to Rating times **1000** waking
> passengers.
>
> **Passengers** (frozen max): Cold passage is the most common method of
> transit in space. Frozen passengers require little-to-no handling or
> maintenance once they are brought aboard. Each Cold Passenger, along
> with a freeze tube and its associated support systems, requires **1**
> tonne of Cargo space.

<br />

**Crew**: The number of Crew required on a vessel (if any) highly
depends upon the vessel and its function (Craft, for instance, have no
Crew aboard). As a rule of thumb, the more advanced or better-made the
vessel, the more automated it tends to be.

Other factors include the number of waking Passengers it can carry
(frozen Passengers are generally handled like Cargo), and the amount of
Cargo space the vessel possesses. Specialized vehicles, such as
emergency response launches, construction boats, courier ships, etc.,
have precise Crew and mission specialist requirements.

As a result, only general estimates for Crew numbers are possible, and
these are determined as follows:

> 10 minus Rating (minimum of 1), **and**...

> 1 per every 10,000 tonnes Cargo carried, **and**...

> 1 per every 50,000 tonnes of the vessel's Size, **and**...

> 1 per every 10 waking Passengers.

Partial amounts are rounded up (*e.g.*, having between 11 and 20 waking
Passengers requires 2 Crew; having between 10,001 and 20,000 tonnes of
Cargo means 2 Crew; etc.).

<br />

***

<br />

There are probably hundreds of exceptions to every rule in
this chapter. No two vessels in space are exactly alike, even if they
are of the same model. Owners and crews modify and jury-rig systems all
the time. Things get altered, improved, or broken. And industry
"standards" are sometimes more of a concept, than they are a reality.

<div align="center">

![star-image-400x410](../Images/star-image-400x410.gif "Next stop: Jarden star system!")\

</div>
