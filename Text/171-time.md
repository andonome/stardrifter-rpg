# TIME IN THE STARDRIFTER UNIVERSE

### Galactic Time

As humanity spread out among the stars, the Gregorian Calendar came along with it. In time, though, as people began to settle in far-flug places, all with different day/night cycles, and different (or even non-existent) lengths of year, the months, weeks, and seasons of Old Earth began to seem irrelevant. In time, as local benchmarks began to creep in to the time references of various settlements and colonies, causing miscommunications and accidents, it became clear that something more standardized was needed.

Since most of the modified time-keeping systems were based upon the Gregorian Calendar, the new one, called **Galactic Time (GT)**, was based on it as well, for the sake of familiarity and ease of adoption. **Year 0** of the GT calendar was pegged as the year of the very first starjump, made by the automated vessel ***XA-551*** (never mind that the trip was, in fact, a misjump). This equates to the year 2188 CE in the Gregorian calendar. The new calendar was introduced in 2270 CE, being recursively started from the year when **XA-551** jumped into history. It still took well over a century before it became a standard throughout settled space.

***Stardrifter*** takes place approximately **500 years** in the future, so the GM looking to set a campaign in a particular period is invited to pick a date of their choosing, somewhere in the **26th Century**. That would place it in the 5th Centery Galactic Time, or somewhere in the 400s .

<div align="center">

**Galactic Time vs the Gregorian Calendar**
<br />

![space-doctor-250x209](../Images/gt-vs-gregorian-600x179.gif "Galactic Time vs Gregorian Calendar")

</div>

<br />

The details of how **Galactic Time** works are as follows:

* **Hours** are *60 minutes* long.

* **Days** are *24 hours* long.

* Days are broken up into three sections: 
  *  **firstshift** (04:00 to 11:59hrs).
  *  **midshift** (12:00 to 19:59hrs).
  *  **thirdshift** (20:00 to 03:59 hrs).
<br />

* **Weeks** are numbered numerically, from ***01*** to ***52***.

* **Weeks** are *7 days* long.

* Days of the week are numerically named:
  *  **Oneday** (Sunday)
  *  **Twoday** (Monday)
  *  **Threeday** (Tuesday)
  *  **Fourday** (Wednesday)
  *  **Fiveday** (Thursday)
  *  **Sixday** (Friday)
  *  **Sevenday** (Saturday)
<br />

* **Months** are *not* counted.

* There are *no* **Time Zones**.

* **Years** are *365 days* long, with no leap years.

* Days are designated numerically, from ***Day 001*** to ***Day 365***.

* Years are broken up into Quarters, which are roughly analogous to seasons:
  *  **1st Quarter** (Day 001-091, nicknamed "Fresh").
  *  **2nd Quarter** (Day 092-182, nicknamed "Grind").
  *  **Midyear Day** (Day 183 -- designated holiday, not counted in any Quarter).
  *  **3rd Quarter** (Day 184-274, nicknamed "Leaf")
  *  **4th Quarter** (Day 275-365, nicknamed "Rain")
<br />

* Years are appended as 1GT, 2GT, etc. Years and dates from before Galactic Time are given a negative designation, such as -1GT, -2GT, etc.

* As **Galactic Time** is used pretty-much universally throughout settled space, the year, quarter, date, and time of day are the same everywhere you go. Starjump from one star system to another, and you find that the date and time are exactly the same in both places. Remember, too, that starjump occurs in the real universe instantaneously, (See **Subjective Time** below.) Settlers on planets and large asteroids often adopt their own time designations, corresponding to the local day/night cycles, and even to true seasons, but if some reference to local time is shared with the wider galaxy, its Galactic Time equivelant is always appended.

* The planet **Terra** still uses days, months, seasons, time zones, and leap years pertinent to itself; in other words, it still uses the Gregorian calendar. Large Earth-based companies and governmental agencies that deal with the wider galaxy often append Galactic Time to any communications or documents. Many, if not most companies and people on Terra, however, fail to do this because of a common cultural bias, to the effect of: "Earth's calendar came first, so it matters the most; figure it out for yourself". Old habits can be hard to break, and annoying to deal with.

* Astronomists and other researchers use various time-keeping systems specific to their specialities, but Galactic Time is nonetheless used for everyday purposes.

<br />

### Subjective Time

An important side-aspect of starjump, **Subjective Time (ST)** refers to the temporal differential between those inside the active jump bubble of a starship in flight, and those in the real universe beyond. Starjump occurs instantaneously in the real universe, but it *does* take time for the vessel itself, and the people within, as they travel through the artificial universe created by the ship. (See the chapter on **Starjump**.)

It is typical for starships to keep track of this differential through the use of two onboard clocks. One keeps Galactic Time, and it pauses when the ship enters starjump. A second clock then starts up, tracking time within the jump bubble. It continues to measure the passage of time normally, until the vessel exits jumpspace, whereupon it stops, and the GT clock begins again. All passengers and crew aboard the vessel then have the amount of ST they've experienced appended to their IDent record as a part of their official identification. Birthdays and other important milestones that might happen while in mid-jump are sometimes celebrated twice: once when they happen subjectively, aboard the ship, and a second time, when the GT calendar says it's time to do so in the real universe.

Exactly how much ST passes within (and immediately around) the ship is an endemic aspect of three things:

1. The distance being traveled, in **Light Years**.
2. The quality of starjump engine involved, reflected in the ship's **Rating**.
3. The vessel's bulk, reflected in the ship's **Size**.

The formula to determine this is:

**12** hours *minus* ship's Rating per Light Year traveled, ***and***...
<br />
**6** hours *times* ship's Size

> **Example**: The frame carrier **Steeplechaser** (Size: 2, Rating: 4) sets out for a star system called **Harbordale**, twenty-eight light years away. This is far beyond its starjump range of **8 light years**, so it will have to make a number of stops, trading goods, and getting fuel and service at ports along the way. It has a schedule to keep, but is not in a rush, so there will be no consecutive jumps. Because of the way that settled star systems and tween-star stations are laid out in the general direction of their destination, **Steeplechaser** will have to take a slightly circuitous route, of five starjumps, with a total distance traveled of **34** light years, in order to get to Harbordale. At **12** hours minus **4** equals **8**, times **34** (the number of light years traveled), plus **12** (**6** times the Size of **2**), it's 284 hours. The ship spends almost 12 days, total, in Subjective Time during the cruise, spread out among five starjumps.
