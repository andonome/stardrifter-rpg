# Acknowledgements

The early versions of the ***Stardrifter*** game
mechanics were based, in part, on **Brent P. Newhall's** wonderful, and
wonderfully simple, [Dungeon Raiders](https://www.drivethrurpg.com/product/99366/Dungeon-Raiders?filters=0_2140_0_0_0),
a retroclone of the original [Dungeons & Dragons](https://en.wikipedia.org/wiki/Dungeons_%26_Dragons_(1974))
boxed set. ***Stardrifter*** has since deviated quite a bit from that
original source material, but without it, this game would almost
certainly not exist. Thanks Brent, for all your great work, and for
letting the rest of us play with your toys!

Thanks also to my brave players and playtesters: **Klaatu**, **Thaj**, **Mark**, **Brian**, **x1101**, **Jeff**, **Richard**. and **Daniel**. Their confidence and keen suggestions helped to shape this game.
I could not have done it without you guys!

Special thanks *again* to the ***Stardrifter RPG's*** incredible Chief Engineer, **Lyle McKarns (x1101)**, for his tireless behind-the-scenes work in formatting this book, and turning it into a living document at home in the aether! 

Finally, special thanks, as always, to my family, **Debbie** and **Emmett**, who
put up with all my nonsense. I don't know why they do, but I sure am
happy for it.

<div align="center">

![radio-telescope-250x346](../Images/radio-telescope-250x346.gif "Is there anybody out there?")

</div>
