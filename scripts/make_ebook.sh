#!/usr/bin/env bash

# (C) 2020 LJM, GPLv3
#
## Script to convert Stardrifter RPG markdown to epub

## Definitions
SourceDir="`pwd`/Text"
OutputDir="`pwd`"
BuildOptions="--table-of-contents ${SourceDir}/metadata.txt "
DEBUG=1

## Debugging information
if [[ ${DEBUG} -gt 0 ]]; then
  echo "Working Dir: `pwd`"
  echo "Source Dir: ${SourceDir}"
  echo "Build Options: ${BuildOptions}"
  echo "pandoc location: `which pandoc`"
  echo "Date Stamp: ${DateStamp}"
fi

cd ${SourceDir}
`which pandoc` -o ${OutputDir}/stardrifter.epub `ls *.md` ${BuildOptions}
