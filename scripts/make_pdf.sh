#!/usr/bin/env bash

# (C) 2020 LJM, GPLv3
#
## Script to convert Stardrifter RPG markdown to pdf

## Definitions
SourceDir="`pwd`/Text"
OutputDir="`pwd`"
DEBUG=0

## Debugging information
if [[ ${DEBUG} -gt 0 ]]; then
  echo "Working Dir: `pwd`"
  echo "Source Dir: ${SourceDir}"
  echo "Build Options: ${BuildOptions}"
  echo "pandoc location: `which pandoc`"
fi

# First, loop through the MD files and convert to HTML
cd ${SourceDir}
# In Debug Mode, prove we cd'd 
if [[ ${DEBUG} -gt 0 ]]; then pwd; fi
for f in `ls *.md`; do
  pandoc --output "${f%.md}.html" \
	--from markdown \
	--to html5 \
 	--css custom.css \
	--metadata title=Stardrifter \
	${f}
done

# In Debug Mode, prove we're still where we want to be
if [[ ${DEBUG} -gt 0 ]]; then pwd; fi
# Now combine the HTML to a PDF
pandoc --output ${OutputDir}/stardrifter.pdf \
	--from html \
	--include-in-header chapter_break.tex \
	--include-before-body cover.tex \
	--include-after-body back-cover.tex \
	--variable=geometry:margin=3cm \
	--table-of-contents \
	`ls *.html` 

# And Cleanup
rm *.html
