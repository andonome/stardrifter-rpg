# To Do

- Better guidelines for Skill and Attribute penalties, depending upon certain situations (make this a table?).

- Add difficulty categories for Skills. Some things are much harder to do without any training, so the penalties are doubled/tripled/whatever, if you aren't skilled in them.

- STR bonus damage for combat (need other Attribute bonuses/penalties for other things).

- Movement entry in rules. There's already some mention in Skills > Athleticism, and Combat.

- Char Sheet needs room for special Background details.

- Backgrounds: Spacer > Spacial Abilities: "...only take 1/2 STAM damage (not HP)". Change to "...only takes 1/2 STAM damage. This does not apply to HP damage."

- Skills: Athleticism. "Range is 10 meters plus 2 meters per Skill level." Change to: "Range is 10 meters plus 2 meters per Skill level, without i ncurring any penalty modifiers for distance." Combine/Contrast with stuff in Skills > Combat: Thrown Object.

- Specific entry for thrown objects. Distances/weights (with reference to STR), and penalties. Maybe, no bonus for up close, unless you have Athleticism? Detail crossover with Combat: Thrown Object. Maybe range bonuses are added together if they have both Skills?

- Radio Earbud/Headset includes video comm and media consumption over networks/sideloaded files.

- Equipment > Gear Simple Vacsuit. After any usage, will need a refurb (take 1d4 days, and costs 200Q) or replacement.

- Note on Gender. In this future, people can be Male, female, or Enby (N.B., or Non-binary, which covers transgender, nongender, intersex, or pretty-much everything else). Genescupting allows for complete gender reassignment, including procreative aspects, so a character may change their gender, if desired. This takes time, but not money, as it is covered by pretty-much all medical plans.

- Unexceptional NPCs (UNPCs) and Exceptional NPCs (ENPCs). Need a chapter on NPCs. See top entry on "opal-city-welcomes-you.txt".

- Medico treatment of Stamina: Even one point of healing will return all STAM. If character is getting HP back, once all HP is healed, any number of points left over from that application of Medico roll over into the character's STAM, and will heal STAM completely. Alternatively, if only STAM is missing, any successful use of Medico will heal up all STAM.

- CPs at the start of the game for getting new Skills: maybe allow adding these into other Skill scores, improving Skill scores right off the bat.

- Describe the different types of Comm units, and Earbud Radio/Headset, and what they can do.

- Repairing Armor: Need Engineering. Need to spend 1 hour per each point of DR repaired. Roll Engineering, and it only takes 1/2 hour per point.

- Add chapter on Measurement Standards. Metric is normal, but exceptions exist, and scientific, math, and engineering experts still use lots of different standards. Terra is all over the place. Pockets of AIN, Churchspace, and Noblespace have inconsistant standards. Only Corporatespace uses the metric system universaally, since it is the only monolithic supernation in space.